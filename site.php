<?php

require_once(__dir__."/private/init.php");

$site->register_error_handler();
$site->dispatch(strtok($_SERVER["REQUEST_URI"], '?'));
