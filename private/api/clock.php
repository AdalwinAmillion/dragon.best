<?php

require_once(__dir__."/../lib/api/base.php");


class ClockPage extends MemeImageBasePage
{
    public $formats = ["svg", "png", "jpg"];
    public $description = "Renders a dragon clock.";
    public $params = [
        ["time", "datetime", "Time to display", "time description"],
        ["flip", "bool", "Show the dragon on the left side of the clock"],
        ["sleepy", "bool", "Show a sleepy dragon"],
    ];
    public $main_page = "/time/";

    private $tx = 181.00151;
    private $ty = 330.94797;

    function fetch_data($format)
    {
        $datetime = $this->get_parameter("time");

        global $site;
        $image = new SimpleSvgImage();
        $image->load($site->settings->root_dir . "/media/img/vectors/clock.svg");

        $hours = (float)$datetime->format("H");
        $minutes = (float)$datetime->format("i");
        $seconds = (float)$datetime->format("s");
        $minutes += $seconds / 60;
        $hours += $minutes / 60;

        $this->set_hand($image, "hand_hours", $hours / 12);
        $this->set_hand($image, "hand_minutes", $minutes / 60);

        if ( $this->get_parameter("sleepy") )
        {
            $image->getElementById("eyes")->setAttribute("style", "display: none");
        }

        if ( $this->get_parameter("flip") )
        {
            $this->scale($image, "durg", -1, 1);
            $dx = 512 - 2 * $this->tx;
            $image->documentElement->setAttribute("viewBox", "-$dx 0 512 512");
        }

        return $image;
    }

    function set_hand($document, $id, $fraction)
    {
        $angle = 360 * $fraction;
        $document->getElementById($id)->setAttribute(
            "transform",
            "rotate($angle,{$this->tx},{$this->ty})"
        );
    }

    function scale($document, $id, $x=1, $y=1)
    {
        $element = $document->getElementById($id);
        $element->setAttribute("transform",
            "translate({$this->tx}, {$this->ty}) scale($x, $y) translate(-{$this->tx}, -{$this->ty})");
    }
}

$api_page = new ClockPage();

