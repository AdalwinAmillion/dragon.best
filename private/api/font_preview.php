<?php

require_once(__dir__."/../lib/api/base.php");

class FontPreviewPage extends MemeImageBasePage
{
    public $formats = ["png"];
    public $description = "Renders a font.";
    public $params = [
        ["text", "string", "String to render", "text", ""],
        ["font", "font", "Font to use", "name", "DejaVuEmoji"],
        ["background", "color", "Background color", null, "transparent"],
        ["color", "color", "Text color", null, "black"],
    ];

    function fetch_data($format)
    {
        # Config
        $margin = 10;
        $font_size = 24;

        # Params
        $background = $this->get_parameter("background");
        $color = $this->get_parameter("color");
        $font = $this->get_parameter("font");
        $string = $this->get_parameter("text");
        if ( strlen($string) == 0 )
            $string = basename($font, ".ttf");

        # Text sizing
        $image = new Imagick();
        $draw = new ImagickDraw();
        $draw->setFont($font);
        $draw->setFontSize($font_size);

        $tw = new ImagickTextWrapper($image, $draw, $margin, $margin, null);
        $tw->wrap_text($string, 0, 1);

        # Base Image
        $width = round($tw->bounds->width() + $margin * 2);
        $height = round($tw->bounds->height() + $margin * 2);

        # Text (out)
        $image->newImage($width, $height, $background);
        $draw->setFillColor($color);
        $draw->setStrokeColor("transparent");
        $draw->setStrokeWidth(0);
        $tw->draw();

        return $image;
    }
}

$api_page = new FontPreviewPage();

