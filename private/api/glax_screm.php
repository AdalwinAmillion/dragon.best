<?php

require_once(__dir__."/../lib/api/base.php");

class GlaxScremPage extends MemeImageBasePage
{
    public $formats = ["gif", "png", "mp4", "jpg"];
    public $description = "Renders Glax screaming.";
    public $params = [
        ["text", "string", "String to render", "text", "AAAA"],
        ["font", "font", "Font to use", "name", "DejaVuEmoji"],
        ["flip", "bool", "Whether to flip the image", null, true],
        ["ensure_space", "bool", "Ensure spacing between text end and beginning", null],
        ["inhale", "bool", "Whether the text should go inside Glax", null],
        ["colorize", "color", "Shift colors to match this", "color", "transparent"],
    ];

    function __construct()
    {
        parent::__construct();
        $this->img_path = __dir__ . "/assets/glax_screm/";

        $durgs = [];
        foreach ( scandir($this->img_path) as $filename )
            if ( substr($filename, -4) == ".svg" )
                $durgs[] = substr($filename, 0, -4);
        $this->params["durg"] = new ApiParameter("durg", "raw", "Durg screaming", null, "glax", $durgs);
    }

    function parse_args($format)
    {
        $params = new stdClass();

        $params->colorize = $this->get_parameter("colorize");
        $params->ensure_space = $this->get_parameter("ensure_space");
        $params->font = $this->get_parameter("font");
        $params->flip = $this->get_parameter("flip");
        $params->durg = $this->get_parameter("durg");
        $params->inhale = $this->get_parameter("inhale");
        $params->background = $format == "mp4" || $format == "jpg" ? "white" : "transparent";

        $string = str_replace("\n", "", $this->get_parameter("text"));
        if ( unprefix($string, "--altfont") )
            $this->params["font"]->default = "Maya-Emoji";

        if ( unprefix($string, "--colorize") )
            parse_colorize($string, $params->colorize);

        $params->string = $string;

        return $params;
    }

    function fetch_data($format)
    {
        # Params
        $params = $this->parse_args($format);

        # Config
        global $palette;
        $font_size = 64;
        $font_scale = 1;
        $font_border = 8;
        $font_bold = 2;

        # Face sizing
        $input = new SimpleSvgImage();
        $input->load("{$this->img_path}{$params->durg}.svg");
        $text_box = BoundingBox::from_svg($input->getElementById("text_box"));
        $text_box_max_x = (float)$input->getElementById("text_box_max_x")->getAttribute("x");
        $main_color = new ImagickPixel(
            $input->getElementById("main")
            ->getElementsByTagName("stop")->item(0)
            ->style_feature("stop-color")
        );

        $layer = $input->getElementById("layer_durg_bottom");
        $layer->setAttribute("style", "display: none");
        $image_f = $input->to_imagick();
        $layer->setAttribute("style", "display: inline");
        $layer = $input->getElementById("layer_durg_top");
        $layer->setAttribute("style", "display: none");
        $image_b = $input->to_imagick();
        $width = $input->documentElement->getAttribute("width");
        $height = $input->documentElement->getAttribute("height");
        $image_b->scaleImage($width, $height);
        $image_f->scaleImage($width, $height);
        if ( $params->flip )
        {
            $image_f->flopImage();
            $image_b->flopImage();
        }

        if ( $params->colorize->getColorValue(Imagick::COLOR_ALPHA) )
        {
            list($im_deltabri, $im_deltasat, $im_deltahue) = colorize_params(
                $main_color,
                $params->colorize
            );
            $image_b->modulateImage($im_deltabri, $im_deltasat, $im_deltahue);
            $image_f->modulateImage($im_deltabri, $im_deltasat, $im_deltahue);
        }

        # Text sizing
        $image_text = new Imagick();
        $draw = new ImagickDraw();
        $draw->setFont($params->font);
        $draw->setFontSize($font_size);

        $font_scale = determine_font_scale(
            $image_text,
            $draw,
            $params->string,
            $text_box->width(),
            $text_box->height(),
            $font_bold + $font_border,
            0,
            3
        );

        // re-evaluate instead of setting min scale to 1 to ensure tall glyphs
        // are scaled down, also let's reduce the max scale a bit
        if ( $font_scale < 1 )
            $font_scale = determine_font_scale(
                $image_text,
                $draw,
                $params->string,
                null,
                $text_box->height(),
                $font_bold + $font_border,
                0,
                2
            );

        $draw->setFontSize($font_size*$font_scale);
        $tw = new ImagickTextWrapper($image_text, $draw, 0, 0, null);
        $tw->wrap_text($params->string);

        $full_border = ($font_bold+$font_border)*$font_scale;
        $text_x = $full_border / 2;
        $text_y = $full_border / 2;

        $atw = $tw->bounds->width() + $full_border;
        $ath = $tw->bounds->height() + $full_border;
        $atx = $text_box->x1 + ($text_box->width() - $atw) / 2;
        $aty = $text_box->y1 + ($text_box->height() - $ath) / 2;
        if ( $atx > $text_box_max_x )
            $atx = $text_box_max_x;
        $actual_text_box = new BoundingBox($atx, $aty, max(512, $atx+$atw) , $aty + $ath);

        if ( $params->flip )
        {
            $actual_text_box->translate(-$atx, 0);
            $text_box->translate(-$text_box->x1, 0);
        }

        if ( $params->ensure_space )
        {
            $spacefm = $tw->font_metrics(" ");
            $spacew = $spacefm["originX"];
            $delta = $atw + $spacew - $actual_text_box->width();
            if ( $delta > 0 )
            {
                if ( $params->flip )
                    $actual_text_box->x2 += $delta;
                else
                    $actual_text_box->x1 -= $delta;
            }
        }

        $requires_text_surface = $actual_text_box->width() > $text_box->width();

        // Duration in seconds
        $duration = max(0.75, min(3, 0.75 * $actual_text_box->width() / $text_box->width()));
        $fps = 50;
        $nframes = round($fps * $duration);

        $image_text->newImage($actual_text_box->width(), $actual_text_box->height(), 'transparent');

        # Text (out)
        $fill = $palette->iris->to_html();
        $outline = $palette->iris_dark->to_html();
        $draw->setFillColor($outline);
        $draw->setStrokeColor($outline);
        $draw->setStrokeWidth(($font_bold+$font_border)*$font_scale);
        $tw->draw($text_x, $text_y);

        $draw->setFillColor($fill);
        $draw->setStrokeColor($fill);
        $draw->setStrokeWidth($font_bold*$font_scale);
        $tw->draw($text_x+1, $text_y);

        // Combine
        $image = new Imagick();
        $image->setBackgroundColor('transparent');
        $delta = $actual_text_box->width()/$nframes;
        if ( $params->flip ^ $params->inhale )
            $delta = -$delta;

        if ( $requires_text_surface )
        {
            $text_surface = new Imagick();
            $atw = $actual_text_box->width();
            $ts_text_x = $text_box->width() - $atw;
        }

        for ( $i = 0; $i < $nframes; $i++ )
        {
            $image->newImage($width, $height, $params->background);
            $head_x = rand(-6, 6);
            $head_y = rand(-6, 6);

            $image->compositeImage($image_b, imagick::COMPOSITE_DEFAULT, $head_x, $head_y);

            $image_text->rollImage($delta * $i, 0);
            if ( $requires_text_surface )
            {
                $text_surface->newImage($text_box->width(), $actual_text_box->height(), "transparent");
                $text_surface->compositeImage($image_text, imagick::COMPOSITE_DEFAULT, $ts_text_x, 0);
                $image->compositeImage($text_surface, imagick::COMPOSITE_DEFAULT, $text_box->x1, $actual_text_box->y1);
                $text_surface->clear();
            }
            else
            {
                $image->compositeImage($image_text, imagick::COMPOSITE_DEFAULT, $actual_text_box->x1, $actual_text_box->y1);
            }
            $image_text->rollImage(-$delta * $i, 0);

            $image->compositeImage($image_f, imagick::COMPOSITE_DEFAULT, $head_x, $head_y);

            $image->setImageDispose(2);
            $image->setImageDelay(round($duration * 100 / $nframes));
        }
        return $image;
    }
}

$api_page = new GlaxScremPage();
