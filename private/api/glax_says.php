<?php

require_once(__dir__."/../lib/api/base.php");

class GlaxSaysPage extends MemeImageBasePage
{
    public $formats = ["png", "jpg", "mp4", "gif"];
    public $description = "Renders a Glax sticker with text.";
    public $params = [
        ["text", "string", "String to render", "text", "..."],
        ["bottom_text", "string", "String to render at the bottom", "text", ""],
        ["background", "color", "Image background color", null, "transparent"],
        ["wave", "bool", "Whether to apply a wave effect"],
        ["font", "font", "Font to use", "name", "DejaVuEmoji"],
        ["colorize", "color", "Shift colors to match this", "color", "transparent"],
        ["width", "int", "Maximum image width"],
        ["rainbow", "int", "Make it gayer", null, 0, ["disabled"=>0, "text"=>1, "full"=>2]],
    ];

    function __construct()
    {
        parent::__construct();

        $this->face_abs_path = __dir__ . "/assets/glax_says/";
        $faces = [];
        foreach ( scandir($this->face_abs_path) as $filename )
            if ( strlen($filename) > 4 && substr($filename, -4) == ".png" )
                $faces[] = substr($filename, 0, -4);
        $this->params["face"] = new ApiParameter("face", "raw", "Face to use", null, "unimpressed", $faces);
    }

    function render_rext(
        $string,
        $width,
        $height,
        $font,
        $font_size,
        $font_bold,
        $font_border,
        $line_height,
        $margin
    )
    {
        global $palette;
        # Text sizing
        $image = new Imagick();
        $draw = new ImagickDraw();
        $draw->setFont($font);

        $font_scale = 1;

        $draw->setFontSize($font_size);
        if ( strpos($string, "\n") === false )
        {
            $font_scale = determine_font_scale(
                $image,
                $draw,
                $string,
                $width,
                $height,
                $font_bold + $font_border,
                1,
                3
            );
        }
        $draw->setFontSize($font_size*$font_scale);

        $half_border = ($font_bold+$font_border)*$font_scale / 2;

        $text_x = $half_border;
        $text_y = $margin + $half_border;
        $text_w = $width - $half_border * 2;
        $tw = new ImagickTextWrapper(
            $image,
            $draw,
            $half_border,
            $margin + $half_border,
            $text_w
        );
        $tw->wrap_text($string, 0.5, $line_height);
        $image->newImage($width, $tw->bounds->y2 + $half_border + 1, 'transparent');

        # Text (out)
        $fill = $palette->iris->to_html();
        $outline = $palette->iris_dark->to_html();
        $draw->setFillColor($outline);
        $draw->setStrokeColor($outline);
        $draw->setStrokeWidth(($font_bold+$font_border)*$font_scale);
        $tw->draw(-1);

        # Text (fill)
        $draw->setFillColor($fill);
        $draw->setStrokeColor($fill);
        $draw->setStrokeWidth($font_bold*$font_scale);
        $tw->draw();

        return $image;
    }

    function parse_args()
    {
        $params = new stdClass();

        $params->wave = $this->get_parameter("wave");
        $params->face = $this->get_parameter("face");
        $params->colorize = $this->get_parameter("colorize");
        $params->rainbow = $this->get_parameter("rainbow");
        $params->bottom_string = $this->get_parameter("bottom_text");
        $params->background = $this->get_parameter("background");

        $string = $this->get_parameter("text");

        while ( $string && $string[0] == "-" )
        {
            if ( unprefix($string, "--altfont") )
                $this->params["font"]->default = "Maya-Emoji";
            else  if ( unprefix($string, "--colorize") )
                parse_colorize($string, $params->colorize);
            else if ( unprefix($string, "--spaced") )
                $params->spaced = true;
            else if ( unprefix($string, "--rainbow2") )
                $params->rainbow = 2;
            else if ( unprefix($string, "--rainbow") )
                $params->rainbow = 1;
            else
                break;
        }

        if ( !$string )
        {
            $string = "...";
        }
        else if ( $params->bottom_string == "" && strpos($string, "::") !== false )
        {
            list($string, $params->bottom_string) = array_map("trim", explode("::", $string, 2));
        }

        $params->string = $string;

        $params->font = $this->get_parameter("font");

        $params->scale_width = $this->get_parameter("width");
        if ( $params->scale_width == null )
            $params->scale_width = 512;

        return $params;
    }

    function fetch_data($format)
    {
        # Config
        $margin = 10;
        $font_size = 64;
        $font_border = 8;
        $line_height = 1;
        $font_bold = 0;
        $face_dw = 512;
        $face_pad = $face_dw / 4;
        $width = $face_pad * 2 + $face_dw;
        $rainbow_offset = rand() / getrandmax() * 2 - 1;

        # Params

        $params = $this->parse_args();
        if ( substr($params->font, -14, -4) == "Maya-Emoji" )
        {
            $font_bold = 2;
            $line_height = 0.66;
        }

        # Face sizing
        $filename = "{$this->face_abs_path}{$params->face}.png";
        $face_image = new Imagick($filename);
        $face_w = $face_image->getImageWidth();
        $face_h = $face_image->getImageHeight();
        $face_dh = (int)round($face_dw / $face_w * $face_h);
        $face_image->scaleImage($face_dw, $face_dh);


        $img_text = $this->render_rext(
            $params->string,
            $width,
            $face_pad,
            $params->font,
            $font_size,
            $font_bold,
            $font_border,
            $line_height,
            $margin
        );
        $face_y = $img_text->getImageHeight() + $margin / 2;
        $height = $face_y + $face_dh;
        $face_x = $face_pad;

        if ( $params->bottom_string )
        {
            $bottom_img_text = $this->render_rext(
                $params->bottom_string,
                $width,
                $face_pad,
                $params->font,
                $font_size,
                $font_bold,
                $font_border,
                $line_height,
                $margin
            );
            $bottom_img_y = $height;
            $height += $bottom_img_text->getImageHeight() + $margin / 2;
        }

        # Colorize
        if ( $params->colorize->getColorValue(Imagick::COLOR_ALPHA) )
        {
            global $palette;
            list($im_deltabri, $im_deltasat, $im_deltahue) = colorize_params(
                new ImagickPixel($palette->body_main),
                $params->colorize
            );
            $face_image->modulateImage($im_deltabri, $im_deltasat, $im_deltahue);
        }

        # Animation
        $duration = 1;
        $nframes = 1;
        $rainbowizer = null;
        if ( $this->is_animated($format) )
        {
            if ( $params->rainbow || $params->wave )
                $nframes = 10;
            $rainbow_offset = -1/$nframes;
        }

        if ( $params->rainbow )
        {
            $rainbowizer = new Rainbowizer($width, $height, "#05f-#f5f", $rainbow_offset);

            $img_text->modulateImage(150, 100, 100);
            $rainbowizer->add_image($img_text);

            if ( $params->rainbow >= 2 )
                $rainbowizer->add_image($face_image);

            if ( $params->bottom_string )
            {
                $bottom_img_text->modulateImage(150, 100, 100);
                $rainbowizer->add_image($bottom_img_text);
            }
        }

        $image = new Imagick();
        for ( $i = 0; $i < $nframes; $i++ )
        {
            if ( $params->rainbow )
                $rainbowizer->gradient->rollImage($width * abs($rainbow_offset), 0);

            # Base Image
            $image->newImage($width, $height, $params->background);

            # Text
            if ( $params->rainbow )
                $rainbowizer->apply($img_text, 0, 0);
            $image->compositeImage($img_text, imagick::COMPOSITE_DEFAULT, 0, 0);


            if ( $params->bottom_string )
            {
                if ( $params->rainbow )
                    $rainbowizer->apply($bottom_img_text, 0, 0);
                $image->compositeImage(
                    $bottom_img_text, imagick::COMPOSITE_DEFAULT, 0, $bottom_img_y
                );
            }

            # Face
            if ( $params->rainbow >= 2 )
                $rainbowizer->apply($face_image, -($width - $face_dw)/2, 0);

            $image->compositeImage(
                $face_image,
                imagick::COMPOSITE_DEFAULT,
                $face_x,
                $face_y
            );

            if ( $params->wave )
            {
                $image->rollImage(-$width * abs($i/$nframes), 0);
                $image->waveImage(48, $width);
                $image->rollImage($width * abs($i/$nframes), 0);
            }

            $image->setImageDelay($duration * 100 / $nframes);

            if ( $params->scale_width < $width )
                $image->scaleImage($params->scale_width, 0);

            if ( $params->wave )
                $image->setImageDispose(2);
        }

        return $image;
    }
}

$api_page = new GlaxSaysPage();
