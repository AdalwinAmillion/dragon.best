<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../commission-list.php");


class CommissionPage extends DurgPage
{
    public $title = "Commissions";
    public $description = "Commission info for Glax";
    public $default_image = "/media/img/rasterized/vectors/icons/icon_image.png";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css"
    ];
    public $scripts = [
        "https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.5.3/lottie.js"
    ];

    private function heading($tag, $text, $anchor=null)
    {
        $head = new HeadingAnchor($tag, $text, $anchor);
        echo $head;
        return $head;
    }

    private function p($text)
    {
        echo mkelement(["p", [], $text]);
    }

    function tos()
    {
        $this->heading("h2", "Terms of service", "tos");

        $this->heading("h3", "Definitions");
        $this->p("You: the person or entity requesting the commission.");
        $this->p("Me, I: Glax, the artist You are commissioning.");
        $this->p("Work: the work produced by Glax for You under these terms.");

        $this->heading("h3", "General");
        $this->p("By commissioning Me, You agree to these terms in full.");
        $this->p("Unless otherwise specified, these terms apply to any work done by Me for You.");
        $this->p("These terms may change at any time without notice, the version available when the commission is accepted by Me.");
        $this->p("I will keep a very short queue of commissions pending completion. ".
            "If You fail to get a commission slot because the queue is full, feel free to ask again at a later time.");

        $this->heading("h3", "Disclaimer of Warranty and Limitation of Liability", "warranty");
        $this->p("The Work is provided as-is, without any warranty of any kind, either expressed of implied.");
        $this->p("In no event will I be held liable to You for any damages arising out of these terms or uses of the Work.");

        $this->heading("h3", "Subject Matters");
        $this->p("Any characters to be depicted in the Work must be owned by You or You must have explicit permission to use them.");

        $this->heading("h3", "Commission Process");

        $this->p("To enquire about available commissions You can message Me using any of the following contact methods:");
        $con = [
            [["fab", "telegram"],   "@MattBas on Telegram",     "https://t.me/MattBas", "(preferred)"],
            [["fab", "twitter"],    "@GlaxDurg on Twitter",     "https://twitter.com/GlaxDurg", ""],
            [["fab", "deviantart"], "mattbas on DeviantArt",    "https://mattbas.deviantart.com/", ""],
            [["fas", "paw"],        "mattbas on FurAffinity",   "https://www.furaffinity.net/user/mattbas/", ""],
        ];
        echo "<ul>";
        foreach ( $con as $t )
        {
            echo mkelement(["li", [], [
                fa_icon($t[0][1], $t[0][0]),
                new PlainLink($t[2], $t[1]),
                " ",
                $t[3]
            ]]);
        }
        echo "</ul>";

        $this->p("You are responsible for providing all required information regarding the details of the commission.");
        $this->p("You have to provide clear visual references for characters or other specific subjects in the commission.");

        $this->p("The prices listed in this page are a baseline, ".
            "for especially complex design the commission price might be higher. ".
            "In such a case I will notify You of the price for the commission.");

        $this->p("I reserve the right to decline commissions for any reason.");

        $this->heading("h3", "Requesting Changes");
        $this->p(["Changes related to obvious errors made by Me will be free of charge ".
            "(this includes missing part of the references, or details discussed explicitly).", ["br", [], null],
            "This offer is time-limited after the completion of the Work."]);
        $this->p("If references are ambiguous or You provide conflicting references, mismatching details in the Work don't count as errors.");
        $this->p("A small amount of simple changes to the Work might be acceptable, but it's up to Me to decide whether they will be done for free.");
        $this->p("If You request complex changes or several simple changes to the Work, there might be additional fees required for Me to perform such changes.");

        $this->heading("h3", "Payment");
        $this->p("All prices are in Euros, payments are accepted via PayPal.");
        $this->p("You are expected to pay in full before I start working on the commission.");
        $this->p("Once I start working on a commission for You, the paid price becomes non-refundable.");
        $this->p("If I cancel a commission for reasons not related to You, You will get a full or partial refund, depending on the circumstances.");
        $this->p("Excessive complaints about prices will result in You being blacklisted from receiving any commission.");
        $this->p("Breaking these terms might cause the commission to be dropped and payment will be forfeit.");

        $this->heading("h3", "License");

        $this->heading("h4", "General licensing terms");

        $this->p(["The Work will be provided to You with a non-exclusive license to publish and modify the Work ",
            "but You must always provide attribution and You must retain the license in modified copies of the Work."
        ]);

        $this->p("I retain all rights to the Work, including but not limited to the rights of publishing it, making changes to it, and profiting from it.");

        $this->p("If You wish to have a different license agreement, this must be stated before I start working on the commission, as this might incur in a higher fee.");


        $license_audiovisual = $this->heading("h4", "License for Audio-Visual Media");
        $this->p("This section describes the licensing terms for audio-visual components of the Work.");

        $this->p(["You will receive the Work under the terms of the ", License::find_license("CC BY-NC-SA")->as_link(), " license."]);
        $this->p("The binding text of the license is provided in the link above, a non-binding summary follows:");
        ?><ul>
            <li>You have the right to publish the Work, given You provide attribution</li>
            <li>You cannot use the Work for commercial purposes</li>
            <li>You have the right to make changes to the Work, but You must indicate that changes were made</li>
            <li>If You distribute modified copies of the Work, the derived work must be under the same license</li>
        </ul><?php

        $this->p([
            "I will also distribute the Work to the public under the teroms of the ",
            License::find_license("CC BY-NC-ND")->as_link(), " license. ",
            "This means other people will have access to the Work but they will not be able to make changes."
        ]);


        $this->heading("h4", "License for Source Code");
        $this->p("This section describes the licensing terms for the source code of the Work (this applies for bot commissions).");
        $this->p(["You will receive the Work under the terms of the ", License::find_license("AGPLv3+")->as_link(), " license."]);
        $this->p("The binding text of the license is provided in the link above, a non-binding summary follows:");
        ?><ul>
            <li>You have the right to publish the Work, given You provide attribution</li>
            <li>You can use the Work for commercial purposes</li>
            <li>You have the right to make changes to the Work, but You must indicate that changes were made</li>
            <li>If You distribute modified copies of the Work, the derived work must be under the same license</li>
        </ul><?php
        $this->p([
            "The terms are similar as with audio-visual components of the Work. ",
            "You have the added right of being able to use the source code for commercial purposes ",
            "but You must always provide notices of license and attribution."
        ]);

        $this->heading("h3", "Telegram Bots");

        $this->p("This section describes some terms that only apply to telegram bot commissions.");

        $this->p(["You are responsible for registering the bot on Telegram and getting an API Token. For Your convenience, here's a ",
            new PlainLink("#faq-tg-create-bot", "FAQ item explaining the process"),"."]);

        $this->p("You have the choice between getting a source-only commission and a hosted bot commission.");
        $this->p("With a source-only commission, You get a bot with its source code but You are responsible for running the bot.");
        $this->p([
            "With a hosted bot commision, Your bot will run on My server, with an additional cost. ",
            "Note that I will maintain my server but you will not obtain any additional support or warranty on the bot's uptime.", ["br", [], null],
            "With this option You must provide a valid Telegram API Token to be used for the bot."
        ]);
        $this->p("This choice must be expressed before the commission is accepted as hosted options will have an additional cost.");

        $this->p("Additions or changes to the source code can be requested after the bot is completed but they might incur in additional fees.");

        $this->p("Additional support for hosted bots can be arranged but it will incur in additional fees.");

        $this->p(
            "If as part of the bot commission additional media components are created (for example a base sticker that the bot uses to generate its content) ",
            "You will receive said media components under the same terms You would get if You commissioned those components on their own."
        );

        $this->p([
            "The bot output is licensed under the terms described in the section ", $license_audiovisual->link(), "."
        ]);

    }

    function faq()
    {
        $this->heading("h2", "FAQ");

        $this->heading("h3", "How do I create stickers to a pack in Telegram?", "faq-tg-create-pack");
        $this->p([
            "Talk to the ", new Link("https://t.me/Stickers", "@Stickers"), " bot. ",
            "It has various commands to create or add stickers to your packs."
        ]);
        $this->p([
            "Note that Telegram doesn't support mixing static and animated tickers in the same pack.",
            "You have to create animated packs with a different command (", ["tt", [], "/newanimated"], ")."
        ]);

        $this->heading("h3", "How do I credit you?", "faq-credit");
        $this->p([
            "Linking to my website (", new PlainLink("https://dragon.best"),
            ") or any of the contact profiles mentioned in the terms is enough in most cases."
        ]);
        $this->p("If adding stickers to your pack on Telegram, you must include my credit stickers:");
        $dloads = [
            "Animated Credits Sticker" => "credits-animated.tgs",
            "Static Credits Sticker" => "credits-sticker.png",
        ];
        echo "<ul>";
        foreach ( $dloads as $name => $file)
        {
            echo mkelement(["li", [], [["a", ["href"=>href("/media/img/pages/credits/$file?download")], [fa_icon("file-download"), $name]]]]);
        }
        echo "</ul>";


        $this->heading("h3", "How do I register bots in Telegram?", "faq-tg-create-bot");
        $this->p([
            "Talk to the ", new Link("https://t.me/BotFather", "@BotFather"), " bot. ",
            "It has various commands to manage your bots."
        ]);

        $this->p([
            "It has the command ", ["tt", [], "/newbot"], " that guides you to the bot creation process, ",
            "at the end of which it will provide with an API Token that you must use in order for the bot to connect to Telegram."
        ]);

        $this->p(["You can access the API Token or make changes to your bots at any time navigating the ", ["tt", [], "/mybots"], " menus."]);

    }


    function extra_head($render_args)
    {
        ?>
        <style>
        .comm-note {
            border: 1px solid #c22;
            padding: 1ex;
            border-radius: 3px;
            background: #fee;
            color: #900;
        }
        .comm-note > .fa-fw {
            font-size: x-large;
            margin-right: 1ex;
        }
        .comm-preview {
            min-height: 256px;
            max-height: 100vw;
            width: 256px;
            display: inline-flex;
            justify-content: center;
            margin-right: 2ex;
        }
        .comm-preview .lottie {
            width: 256px;
            height: 256px;
        }
        .comm-details {
            display: inline-block;
            vertical-align: top;
        }
        .comm-closed {
            color: #900;
        }
        .comm-open {
            color: #090;
        }
        .comm-price {
            font-weight: bold;
        }
        .comm-preview img {
            object-fit: contain;
            width: 256px;
        }
        </style><?php
    }

    function main($render_args)
    {
        $types = commission_types();

        $this->body_title($this->description, $render_args);

        echo mkelement(["h2", [], "Price Info"]);

        $open = false;
        foreach ( $types as $type )
        {
            if ( $type->open )
            {
                $open = true;
                break;
            }
        }

        if ( !$open )
            echo mkelement(["div", ["class" => "comm-note"], [
                fa_icon("exclamation-triangle"),
                "All commission types are currently closed"
            ]]);

        foreach ( $types as $type )
        {
            $type->render();
        }

        $this->tos();
        $this->faq();

    }
}

$page = new CommissionPage();

