<?php

require_once(__dir__."/../../dragon.php");
require_once(__dir__."/../../models.php");


class DragonsSizePage extends DurgPage
{
    public $title = "Size chart of the best dragons";
    public $metadata = [
    ];
    public $scripts = [
        "https://hammerjs.github.io/dist/hammer.min.js",
        "/media/scripts/size.js",
    ];
    public $styles = [
        "/media/styles/durg.css",
    ];
    function extra_head($render_args)
    {
        ?><style>
        #sizechart {
            height: 500px;
            max-height: 90vh;
            width: 100%;
            border: 1px solid gray;
        }
        .details {
            position: fixed;
            border: 1px solid black;
            background: white;
            pointer-events: none;
            display: none;
        }
        .details header img {
            width: 64px;
            height: 64px;
        }
        .details header {
            font-weight: bold;
            font-size: large;
            border-bottom: 1px solid black;
            display: flex;
            align-items: center;
        }
        .details header span {
            flex-grow: 1;
            text-align: center;
            margin: 0 1ex;
        }
        .details th {
            text-align: right;
        }
        #sizetable td, #sizetable th {
            border: 1px solid gray;
        }
        #sizetable {
            border-collapse: collapse;
        }
        #sizetable td:nth-last-child(1), #sizetable td:nth-last-child(2) {
            text-align: right;
        }
        #sizetable svg {
            vertical-align: middle;
        }
        #sizetable img {
            width: 32px;
            height: 32px;
            vertical-align: middle;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        global $auth;
        $icon = $auth->user ? htmlentities($auth->user->photo_url) : "";


        foreach ( DragonSize::$types as $key => $obj )
            $obj->slug = $key;
        echo "<script>const dragon_types = ".json_encode(DragonSize::$types).";</script>";

        echo '<svg id="sizechart" xmlns="http://www.w3.org/2000/svg"><defs>';
        foreach ( DragonSize::$types as $key => $obj )
            echo str_replace(
                '<?xml version="1.0" encoding="UTF-8" standalone="no"?>', "",
                file_get_contents(root_dir."/media/img/pages/size/$key.svg")
            );
        echo '</defs></svg>';

        if ( $auth->user )
        {
            ?>
                <ul id="form-start" class="buttons">
                    <li><button onclick="form_show('create');">Create</button></li>
                    <li><button onclick="form_show('modify');">Modify</button></li>
                    <li><button onclick="form_show('delete');">Delete</button></li>
                </ul>
                <form id="edit" style="display: none" onsubmit="form_submit(); return false;" autocomplete="off">
                    <table>
                        <tr id="form-row-dragon">
                            <td><label for="edit-id">Dragon</label></td>
                            <td><select name="id" id="edit-id" oninput="form_dragon();"></select></td>
                        </tr>
                        <tr id="form-row-name">
                            <td><label for="name">Name</label></td>
                            <td><input type="text" name="name" id="name" required="required" /></td>
                        </tr>
                        <tr id="form-row-type">
                            <td><label for="type">Type</label></td>
                            <td><?php echo new Select(
                                array_flip(array_map(function($x){return $x->name;}, DragonSize::$types)),
                                ["name"=> "type", "id" => "type", "oninput" => "form_type();"]
                            ); ?></td>
                        </tr>
                        <tr id="form-row-height">
                            <td><label for="height">Height</label></td>
                            <td><input type="number" name="height" id="height" step="any"/>
                                <?php echo new Select(
                                    ["cm", "m", "ft"],
                                    ["name"=> "height-unit", "id" => "height-unit"]
                                ); ?>
                            </td>
                        </tr>
                        <tr id="form-row-length">
                            <td><label for="length">Length</label></td>
                            <td><input type="number" name="length" id="length" step="any"/>
                                <?php echo new Select(
                                    ["cm", "m", "ft"],
                                    ["name"=> "length-unit", "id" => "length-unit"]
                                ); ?>
                            </td>
                        </tr>
                        <tr id="form-row-color">
                            <td><label for="color">Color</label></td>
                            <td><input type="color" name="color" id="color"/></td>
                        </tr>
                        <tr id="form-row-save">
                            <td colspan="2">
                                <button type="button" onclick="form_hide();">Cancel</button>
                                <button type="submit" id="button-save"  onclick="form_submit(); return false;">Save</button>
                            </td>
                        </tr>
                    </table>
                </form>
                <script>
                    let form_mode = null;
                    let form_start = document.getElementById("form-start");
                    let form = document.getElementById("edit");

                    function form_submit_ok()
                    {
                        svg_chart.update();
                        form_hide();
                    }

                    function form_submit()
                    {
                        var object = {};
                        (new FormData(form)).forEach(function(value, key){
                            object[key] = value;
                        });
                        var form_json = JSON.stringify(object);

                        if ( form_mode == "create" )
                        {
                            api_request("PUT", form_json, form_submit_ok, console.warn);
                        }
                        else if ( form_mode == "modify" )
                        {
                            api_request("PATCH", form_json, form_submit_ok, console.warn);
                        }
                        else if ( form_mode == "delete" )
                        {
                            api_request("DELETE", form_json, form_submit_ok, console.warn);
                        }
                    }

                    function form_toggle_rows(ids)
                    {
                        for ( let row of form.querySelectorAll("tr") )
                            form_toggle_row(row, ids.indexOf(row.id) != -1);
                    }

                    function form_toggle_row(element, show)
                    {
                        element.style.display = show ? "table-row" : "none";
                    }

                    function form_hide()
                    {
                        form_mode = null;
                        form_start.style.display = "flex";
                        form.style.display = "none";
                    }

                    function form_show(mode)
                    {
                        form_mode = mode;
                        form_start.style.display = "none";
                        form.style.display = "block";
                        form.reset();

                        if ( mode == "create" )
                        {
                            form_toggle_rows(["form-row-name", "form-row-type", "form-row-save", "form-row-color"]);
                            document.getElementById("type").value = "derg";
                            form_type();
                            document.getElementById("button-save").innerText = "Create";
                        }
                        else if ( mode == "modify" )
                        {
                            form_toggle_rows(["form-row-dragon", "form-row-save"]);
                            form_dragon_setup();
                            document.getElementById("button-save").innerText = "Save";
                        }
                        else if ( mode == "delete" )
                        {
                            form_toggle_rows(["form-row-dragon", "form-row-save"]);
                            form_dragon_setup();
                            document.getElementById("button-save").innerText = "Delete";
                        }
                    }

                    function form_type()
                    {
                        let type = dragon_types[document.getElementById("type").value];
                        if ( !type )
                            return;

                        form_toggle_row(document.getElementById("form-row-height"), type.height);
                        document.querySelector("#form-row-height label").innerText = type.height;
                        form_toggle_row(document.getElementById("form-row-length"), type.length);
                        document.querySelector("#form-row-length label").innerText = type.length;
                    }

                    function form_dragon_setup()
                    {
                        let select = document.getElementById("edit-id");
                        clear(select);
                        let empty = select.appendChild(document.createElement("option"));
                        empty.disabled = true;
                        empty.appendChild(document.createTextNode("Please wait..."));
                        api_request("GET", null, function(data){
                            let count = 0;
                            for ( let dragon of data )
                            {
                                if ( dragon.owned )
                                {
                                    count++;
                                    let opt = select.appendChild(document.createElement("option"));
                                    opt.appendChild(document.createTextNode(dragon.name));
                                    opt.setAttribute("value", dragon.id);
                                    opt.dragon = dragon;
                                }
                            }
                            empty.innerText = "Select Dragon";
                            if ( count == 0 )
                            {
                                console.warn("No dragons");
                                form_hide();
                            }
                        }, function(data){
                            console.warn(data);
                            form_hide();
                        });
                    }

                    function form_dragon()
                    {
                        if ( form_mode == "modify" )
                        {
                            let select = document.getElementById("edit-id");
                            let dragon = select.options[select.selectedIndex].dragon;
                            if ( dragon )
                            {
                                form_toggle_rows([
                                    "form-row-dragon", "form-row-name",
                                    "form-row-type", "form-row-save",
                                    "form-row-color",
                                ]);
                                for ( let prop of ["type", "name", "length", "height", "color"] )
                                    document.getElementById(prop).value = dragon[prop];
                                document.getElementById("length-unit").value = "cm";
                                document.getElementById("height-unit").value = "cm";
                                form_type();
                            }
                        }
                    }
                </script>
            <?php
        }


        echo "<div id='filters'>";
        foreach ( DragonSize::$types as $name => $type )
            echo mkelement(["div", [], [
                ["input", ["type"=>"checkbox", "autocomplete" => "off", "name"=> $name,
                "checked"=>"checked", "id" => "toggle_$name", "oninput" => "update_filters();"]],
                ["label", ["for" => "toggle_$name"], $type->name]
            ]]);
        echo "</div><table id='sizetable' display='none'></table>";
        ?>
        <script>
            let svg_chart = new SizeChart(
                document.getElementById("sizechart"),
                document.getElementById("sizetable")
            );
            svg_chart.update();

            let filters = document.getElementById("filters");
            function update_filters()
            {
                svg_chart.set_filters(new Set(Array.from(
                    filters.querySelectorAll("input:checked")).map(x => x.name)
                ));
            }
        </script>
        <?php
    }
}

$page = new DragonsSizePage();
