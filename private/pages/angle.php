<?php

require_once(__dir__."/../dragon.php");


class AngleDragonPage extends DurgPage
{
    public $title = "Luxembourgish Angle Dragons";
    public $description = "They are very acute";
    public $default_image = "/media/img/pages/burger-face.png";

    function start_section($title, $id=null)
    {
        if ( $id == null )
            $id = title_to_slug($title);

        echo "<section id='$id'>";
        echo mkelement([
            "header", [], [
                ["h" . $this->level, [], [
                    ["a", ["href"=>"#$id"], $title]
                ]],
            ]
        ]);
        $this->level++;
    }
    function end_section()
    {
        $this->level--;
        echo "</section>";
    }

    function next_section($title, $id=null)
    {
        $this->end_section();
        $this->start_section($title, $id);
    }

    function link_section($title, $id=null)
    {
        if ( $id == null )
            $id = title_to_slug($title);
        echo mkelement(["a", ["href"=>"#$id"], $title]);
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        $this->level = 2;

        $this->start_section("Description");?>
        <img src="/media/img/pages/burger-flipped.png" width="512" height="231" alt="A Luxembourgish Angle Dragon"/>
        <img src="/media/img/pages/angle-generic.png" width="512" height="231" alt="A Luxembourgish Angle Dragon"/>
        <p>Luxembourgish Angle Dragons are a completely legitimate species
           of dragons made up out of acute angles.</p>
        <?php $this->end_section();

        $this->start_section("FAQ");
            $this->start_section("Why Luxembourgish?");
                ?><p>Why not?</p><?php
            $this->next_section("Why Angle?");
                ?><p>Because angles are acute!</p><?php
            $this->next_section("Why Dragon?");
                ?><p>Because dragons are awesome!</p><?php
            $this->next_section("Did anyone really ask these questions?");?>
                <p>No.</p>
            <?php $this->end_section();
        $this->end_section();

        $this->start_section("Physical traits");
            ?><p>Luxembourgish Angle Dragons are composed out of triangles with
           acute angles.</p><?php
            $this->start_section("Angles");
                ?><p>Only acute angles are acceptable, right angles are borderline
                and obtuse ones should be split into smaller acute angles.</p><?php
            $this->next_section("Markings");
                ?><p>W-shaped markings are not acceptable because they look
                like two angles side by side.</p><?php
                $this->start_section("Disallowed");
                    ?><dl>
                        <dt>W-shaped marking</dt>
                        <dd>Disallowed because they make the angles feel less unique.</dd>
                    </dl><?php
                $this->next_section("Questionable");
                    ?><dl>
                        <dt>Two Vs next to each other</dt>
                        <dd>It still looks like a W, don't cheat!</dd>
                        <dt>Two Us next to each other</dt>
                        <dd>A double U is kind of like a W.</dd>
                        <dt>Right side up M</dt>
                        <dd>Kind of looks like an upside-down W.</dd>
                    </dl><?php
                $this->next_section("Unquestionable");
                    ?><dl>
                        <dt>Non-letters</dt>
                        <dd>If it isn't a letter it can't be a W.</dd>
                        <dt>X-shaped markings</dt>
                        <dd>If you know the alphabet, you'll know that an X is not a W.</dd>
                        <dt>Upside down M</dt>
                        <dd>Nothing wrong with it.</dd>
                    </dl><?php
                $this->end_section();
            $this->next_section("Aging");
                ?><p>They stop aging when they are 17, so there can be no porn of them.</p><?php
            $this->next_section("Compliance");
                ?><p>Any Angle Dragons that don't comply with these guidelines
                will be thrown into the
                <?php $this->link_section("pit of eternal suffering"); ?>.</p><?php
            $this->end_section();
        $this->end_section();

        $this->start_section("Lore");
            ?><p>
                This species was created when a Maths teacher cursed their
                students for not paying attention at the trigonometry lesson.
                And we all know that is a sin.
            </p>
            <p>
                If an Angle Dragon is exposed to sunlight they get a tan in one sec.
            </p>
            <?php
            $this->start_section("Afterlife");
                ?><p>When any creature dies, their soul ends up in one of the
                three levels of afterlife:</p><?php
                $this->start_section("Well of Infinite Joy");
                    ?><img src="/media/img/pages/pit.jpg" width="512" height="384" alt="Well of Joy"/>
                    <p>This is where only the true Angle Dragons end up after death.</p>
                    <p>It's always empty because Angle Dragons are immortal anyway.</p><?php
                $this->next_section("Hole of Mild Discomfort");
                    ?><img src="/media/img/pages/pit.jpg" width="512" height="384" alt="Hole of Discomfort"/>
                    <p>This is where non-angle dragons go after they die.
                        It isn't great but who cares about souls of non-angle-dragons anyway?</p><?php
                $this->next_section("Pit of Eternal Suffering");
                    ?><img src="/media/img/pages/pit.jpg" width="512" height="384" alt="Pit of suffering"/>
                    <p>This contains the souls of fake Angle Dragons and nonbelievers.</p>
                    <p>Whoever is shoved in the Pit can never escape and will
                    forever feel pain and shame</p><?php
                $this->end_section();
            $this->end_section();
        $this->end_section();

        $this->start_section("List of known Angle Dragons");
            global $site;
            $path = "/media/img/pages/anglies/";
            $dirname = path_join($site->settings->root_dir, $path);
            foreach(scandir($dirname) as $basename )
            {
                if ( is_file("$dirname$basename") )
                {
                    $name = slug_to_title(substr($basename, 0, -4));
                    $this->start_section($name);
                    echo mkelement(["img", ["src"=>href("$path$basename"), "alt"=>$name]]);
                    $this->end_section();
                }
            }
        $this->end_section();
    }


    function extra_footer($render_args)
    {
        echo "<br/>";
        echo "Original Pit photo by ",
            new Link("https://www.flickr.com/photos/onecog2many/4929374625/", "Michael Hiemstra"),
            " (",
            License::find_license("CC BY"),
            ")"
        ;
    }

}

$page = new AngleDragonPage();

