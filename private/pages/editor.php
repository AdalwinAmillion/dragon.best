<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/gallery.php");

class EditorPage extends DurgPage
{
    use FileGalleryTrait;
    public $title = "Make your own dragon picture";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css"
    ];
    public $scripts = [
        "/media/scripts/file_tools.js",
        "/media/scripts/recolor.js",
    ];

    function base_uri()
    {
        return "/editor/";
    }

    function media_path()
    {
        return "/media/img/pages/editor/";
    }

    function server_image_path()
    {
        return dirname(dirname($this->self_dirname())) . $this->media_path();
    }

    protected function get_css_class($selected)
    {
        if ( $selected == 1 ) return "durgpic selected";
        if ( $selected == 2 ) return "bigdurg";
        return "durgpic";
    }

    protected function image_extensions()
    {
        return array("svg");
    }

    function extra_head($render_args)
    {
        ?><style>
        #editors {
            display: flex;
            flex-flow: row wrap;
            justify-content: space-around;
            align-items: center;
        }
        #editors td:first-child {
            text-align: right;
        }
        #editors td:last-child {
            text-align: left;
        }
        #editors select {
            width: 100%;
        }
        </style><?php
    }

    function render_focused(MediaFileInfo $image, $render_args)
    {
        $links = [
            new Link("#", "SVG", ["id"=>"save_svg"]),
            new Link("#", "PNG", ["id"=>"save_png"]),
            new Link("#", "Randomize", ["onclick"=>"controller.randomize_colors()"]),
            new Link("#", "Reset", ["onclick"=>"controller.reset_colors()"])
        ];

        echo "<script>var controller = new RecolorController(true, true, true);";
        echo "controller.setup_page(controller.init_buttons.bind(controller));";
        echo "</script>";
        echo "<div id='editors'>";
        echo "<div id='palette_editor'></div>";
        echo "<iframe id='image_container' src='{$image->full_url()}' rel='image'></iframe>";
        echo "<div id='feature_editor'></div>";
        echo "</div>";

        echo new LinkList($links, "buttons");
    }


    function main($render_args)
    {
        $focus = $render_args["image"] ?? null;

        if ( !$focus )
        {
            $this->body_title(null, $render_args);
            echo "<p>Here you can personalize some of the images to match other characters.</p>";
            echo mkelement(["p", [], [
                "A simplified version of this is available on more images as the \"Recolor\" feature in the ",
                new PlainLink("/vectors/", "vectors"), " pages."
            ]]);
            echo "<p>Feel free to use the images generated, but do give credit. The license for them is CC BY-SA.</p>";
            echo "<p>Click on one of the images below to start.</p>";
        }

        $this->render_gallery($focus, $render_args);
    }
};

$page = new EditorPage();


