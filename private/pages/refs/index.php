<?php

require_once(__dir__."/../../dragon.php");

class RefsPage extends DurgPage
{
    public $title = "Reference Sheet";
    public $default_image = "/media/img/pages/refsheet_small.png";

    function get_meta_type($render_args)
    {
        return "photo";
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);

        echo mkelement(["p", [], [
            "Here's the ",
            new Link("./palette/", "color palette"),
            ". You can look at my ", new Link("/vectors/", "vectors"), "."
        ]]);

        $image_path = href("/media/img/pages/");

        ?>
         <figure typeof='ImageObject'>
            <a href="<?php echo $image_path; ?>refsheet.png" rel="image" property="contentUrl">
                <img src="<?php echo $image_path; ?>refsheet_small.png"
                alt="Glax Refsheet" width="1230" height="836" style="max-width: 100%;height: auto;" />
            </a>
            <figcaption>
                <span property='character'>Glax</span>
                <span property='name'>reference sheet</span>
                by <span property='author'>Wolfool</span>
                (<a href="https://twitter.com/wolfoolart">Twitter</a>,
                <a href="http://www.furaffinity.net/user/wolfool">FA</a>).
            </figcaption>
        </figure>
        <?php
    }
};

$page = new RefsPage();
