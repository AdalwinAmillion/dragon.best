<?php

require_once(__dir__."/../dragon.php");

class ColorPage extends DurgPage
{
    public $title = "Blue dragons are the best";

    function extra_head($render_args)
    {
        global $palette;
        ?><style>
            html {
                background-color: <?php echo $palette->body_main; ?>;
                color: <?php echo $palette->belly; ?>;
            }

            h1 {
                margin-bottom: 200px;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
    }
};

$page = new ColorPage();

