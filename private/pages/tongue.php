<?php

require_once(__dir__."/../dragon.php");

class RawrPage extends DurgPage
{
    public $title = "Glax's Tongue";
//     public $description = "";
    public $default_image = "/media/img/rasterized/icon/scalable.png";
    public $scripts = [
        "/media/scripts/file_tools.js",
        "/media/scripts/recolor.js",
        "/media/scripts/tongue.js",
    ];

    function extra_head($render_args)
    {
        ?>
        <style>
            #game_area {
                position: relative;
                width: 100%;
                height: 812px;
                overflow: hidden;

                user-select: none;
                touch-action: none;
            }
        </style><?php
    }

    function main($render_args)
    {
        ?>
        <div class="game" typeof="Game">
            <h1 property="name">Glax's Tongue</h1>

            <div>
                <p><input type="file" onchange="image_file_input(event);" /></p>
                <p>
                    <input type="url" placeholder="Image URL" id="url_input" />
                    <button onclick="image_url_input();">From URL</button>
                </p>
            </div>

            <svg
                id="game_area"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                onmousemove="mouse_move(event, this)"
                ontouchmove="touch_move(event, this)"
            >
                <defs>
                <filter id="OutlineEffect" color-interpolation-filters="sRGB">
                    <feMorphology
                        in="SourceAlpha"
                        result="Border"
                        operator="dilate"
                        radius="6"
                    />
                    <feFlood
                        flood-opacity="1"
                        flood-color="#b53147"
                        result="Flood"
                    />
                    <feComposite
                        in="Flood"
                        in2="Border"
                        k2="1"
                        operator="in"
                        result="ColorBorder"
                    />
                    <feComposite
                        in="SourceGraphic"
                        in2="ColorBorder"
                        operator="over"
                        result="composite4"
                    />
                </filter>
                </defs>
                <g transform="translate(0,150)">
                    <g id="scale_target">
                        <image id="target" xlink:href=""
                            x="512" width="512" height="512" />
                        <image xlink:href="/media/img/pages/tongue/tongue_bottom.svg"/>
                        <g
                            id="tongue"
                            fill="#e84d66"
                            stroke="transparent"
                            fill-rule="nonzero"
                            filter="url(#OutlineEffect)"
                        ></g>
                        <image xlink:href="/media/img/pages/tongue/tongue_top.svg"/>
                    </g>
                </g>
            </svg>
        </div>

        <script>
            function move_helper(x, y, svg, target)
            {
                var pt = svg.createSVGPoint();
                pt.x = x;
                pt.y = y;
                var global_pt = pt.matrixTransform(svg.getScreenCTM().inverse());
                var matrix = svg.getScreenCTM().inverse().multiply(target.getScreenCTM()).inverse();
                var local_pt = global_pt.matrixTransform(matrix);
                game.move(local_pt.x, local_pt.y);
            }

            function mouse_move(event, element)
            {
                move_helper(event.clientX, event.clientY, element, game.element);
                event.stopPropagation();
            }

            function touch_move(event, element)
            {
                var box = element.getClientRects()[0];
                var touch = event.changedTouches[0];
                move_helper(touch.clientX, touch.clientY, element, game.element);
                event.stopPropagation();
            }

            function on_resize()
            {
                var width = document.getElementById("game_area").clientWidth;
                var target = 1024;
                if ( width < target )
                {
                    var scale_target = document.getElementById("scale_target");
                    scale_target.setAttribute("transform", `scale(${width/target})`);
                }
            }

            function image_set_url(url)
            {
                document.getElementById("target").setAttributeNS("http://www.w3.org/1999/xlink", "href", url);
            }

            function image_file_input(ev)
            {
                image_receive_files(ev.target.files);
            }

            function image_receive_files(files)
            {
                for ( var i = 0; i < files.length; i++ )
                {
                    var file = files[i];
                    if ( file.type.match("image") )
                    {
                        var reader = new FileReader();

                        reader.onload = function(e2)
                        {
                            image_set_url(e2.target.result);
                        };

                        reader.readAsDataURL(file);
                        return;
                    }
                }
            }

            function image_url_input()
            {
                image_set_url(document.getElementById("url_input").value);
            }

            var game = new TongueGame({
                element: document.getElementById("tongue"),
                element_top: document.getElementById("tongue_top"),
                start: {
                    x: 256,
                    y: 256,
                    radius: 44,
                },
                finish: {
                    x: 1024,
                    y: 256,
                    radius: 16,
                },
                segments: 32
            });

            window.addEventListener("resize", on_resize);
            on_resize();
        </script>
        <?php

    }
}

$page = new RawrPage();

