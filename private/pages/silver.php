<?php

require_once(__dir__."/../dragon.php");

class SilverPage extends DurgPage
{
    public $title = "Silver dragons are the prettiest";
    public $description = "";
    //public $default_image = "/media/img/pages/purple.png";

    function extra_head($render_args)
    {
        global $palette;
        ?><style>
            html {
                background-color: #aaa;
                color: #000;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
    }
};

$page = new SilverPage();
