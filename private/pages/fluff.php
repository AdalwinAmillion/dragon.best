<?php

require_once(__dir__."/../dragon.php");

class FluffPage extends DurgPage
{
    public $title = "Scaly dragons are the best";
    public $description = "Scales are better than fur";

    function extra_head($render_args)
    {
        global $palette;
        ?><style>
            html {
                background-color: <?php echo $palette->body_main; ?>;
                color: <?php echo $palette->belly; ?>;
            }

            main > p {
                margin-bottom: 200px;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
    }
};

$page = new FluffPage();
