<?php

require_once(__dir__."/../../dragon.php");
require_once(__dir__."/../../lib/telegram.php");

class AuthCheckPage extends DurgPage
{
    function render($render_args=array())
    {
        global $auth;

        if ( isset($_GET["next"]) )
        {
            $next = $_GET["next"];
            unset($_GET["next"]);
        }
        else
        {
            $next = "/";
        }

        $user = $auth->login($_GET);
        if ( !$user )
            redirect(href("/auth/login/"));
        redirect(href(to_safe_uri($next)));
    }
}

$page = new AuthCheckPage();
