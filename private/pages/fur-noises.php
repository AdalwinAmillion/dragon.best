<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/gallery.php");
require_once(__dir__."/../lib/telegram.php");


class FurNoisesPage extends DurgPage
{
    use FileGalleryTrait;

    public $title = "Fur Noises";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css"
    ];
    public $default_image = "/media/img/rasterized/vectors/noises.png";

    private $ids_list_ignore = [
        "59fdc87160498aa06dd53d3a", # Done
        "5a13d99a2f510c01966663b2", # On Hold
    ];
    private $id_board = "cd5XHAVf";
    private $api_uri = "https://api.trello.com/1/";
    private $mean_cards_per_day = 0.13;
    private $queue_open = true;

    function base_uri()
    {
        return "/fur-noises/";
    }

    function media_path()
    {
        return "/media/img/fur-noises/";
    }

    function raster_image_path()
    {
        return $this->media_path();
    }

    function server_image_path()
    {
        return dirname(dirname($this->self_dirname())) . $this->media_path();
    }

    protected function image_extensions()
    {
        return array("png");
    }

    function render_focused(MediaFileInfo $image, $render_args)
    {
        $links = [
            "Download" => "{$image->thumbnail_url()}?download",
        ];
        $image->render(
            $this->get_css_class(2),
            ["property"=>"image"],
            []
        );
        $this->print_humanmeta($image);

        echo new LinkList($links, "buttons");
    }

    function humanmeta_template($image)
    {
        return "%(name) noises by %(author) (Characters by their respective owners) &copy; %(copyrightYear) %(license:html)";
    }

    function intro($render_args)
    {
        $this->body_title(null, $render_args);

        /*$cards = $this->get_cards();
        $queued = 0;
        $total_complexity = 0;
        foreach ( $cards as $card )
        {
            if ( !in_array($card->idList, $this->ids_list_ignore) )
            {
                $queued++;
                $total_complexity += $this->get_card_complexity($card);
            }
        }

        $days = ceil($total_complexity / $this->mean_cards_per_day);
        $sets = [
            "noises" => Telegram::instance()->sticker_set("FurNoises"),
        ];*/

        ?>
        <h2>Description</h2>
        <p>These stickers are based on my original <?php
            echo new Link("/vectors/noises/", "dragon noises sticker");
        ?>.</p>

        <?php /*
        <h2>Requests</h2>
        <p>If you want one for your character, you can
            <a href="https://t.me/MattBas">send me a message on Telegram</a>.
        </p>
        <p>
            I make them for free but <?php
                echo new Link("/donate/", "you can leave an optional donation");
            ?>.
        </p>
        <p>There are currently <?php echo $queued; ?> queued stickers,
            it should take approximately <?php echo $days; ?> days to complete them.</p>
        <?php if ( !$this->queue_open ): ?>
            <p>Since I'm currently overwhelmed with requests, my queue is temporarily
            closed. Please wait a couple weeks before asking for a sticker.</p>
        <?php endif ?>
        <h3>What I need</h3>
        <ul>
            <li>A reference or example image of your character</li>
            <li>Species or what *noises* text you want (if not obvious)</li>
            <li>Name of the character (under some circumstances, eg: when your Telegram username is ambiguous)</li>
            <li>If you have a preferred color for the *noises* text, do let me know.
                Usually it's the eye color or some other color present in the ref
                but not already used in the sticker</li>
        </ul>
        <p>If you upload the refs through Telegram, please send them as file /
            uncompressed to avoid the excessive jpeg compression added by Telegram.</p>

        <h3>What you get</h3>
        <ul>
            <li>An entry in the <?php echo $sets["noises"]->full_link(); ?> sticker pack on Telegram.</li>
            <li>A 512x512 PNG image (same as the one used for the sticker).</li>
            <li>(Upon request) a PNG without text and properly centered, suitable to be used as avatar.</li>
            <li>(Upon request) versions with alternative text or other minor changes.</li>
            <li>(Upon request) higher resolution PNGs.</li>
            <li>(Upon request) the source SVG file.</li>
        </ul>

        <h3>Licensing</h3>
        <p>The PNG files you can use anywhere you want, please with attribution where feasible.</p>
        <p>The SVG file is <?php echo License::find_license("CC BY-SA")->as_link(); ?>,
        you can still use it anywhere but attribution is required and derivative works must be under the same license.</p>
        <?php
        */
    }


    function main($render_args)
    {
        $focus = $render_args["image"] ?? null;

        if ( !$focus )
        {
            $this->intro($render_args);
            echo "<h2>Stickers</h2>";

        }

        $this->render_gallery($focus, $render_args);
    }

    function get($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $curl_version = curl_version();
        curl_setopt($ch, CURLOPT_USERAGENT, "curl/{$curl_version['version']}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $contents = curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        curl_close($ch);
        return array($contents, $response);
    }

    function get_cards()
    {
        $url = "{$this->api_uri}boards/{$this->id_board}/cards";
        list($contents, $status) = $this->get($url);
        if ( $status != "200" )
            return [];
        return json_decode($contents);
    }

    function get_card_complexity($card)
    {
        $complexity = 1;
        if ( sizeof($card->labels) > 0 )
        {
            foreach ( $card->labels as $label )
            {
                if ( $label->name == "Needs new template" || $label->name == "Glax" )
                    $complexity += 0.5;
            }
        }
        return $complexity;
    }
}

$page = new FurNoisesPage();
