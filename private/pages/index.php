<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../commission-list.php");

class HomePage extends DurgPage
{
    public $title = "🐉 Best Dragon 🐉";
    public $metadata = [
        "site_name" => "🐉 Best Dragon 🐉",
        "title" => "Glax is best Dragon!",
        "description" => "Glax is a dragon with blue scales",
    ];
    public $default_image = "/media/img/rasterized/vectors/sitting.png";

    function extra_head($render_args)
    {
        global $palette;
        ?><style>
            .durg {
                background: url("/media/img/vectors/sitting.svg") no-repeat center center;
                width: 100%;
                height: 90vmin;
                background-size: contain;
                align-self: center;
                max-height: 1200px;
            }
            h1 {
                margin: 0;
                padding: 0;
                height: 10%;
                width: 100%;
                background: transparent;
                display: flex;
                align-items: center;
                justify-content: center;
                font-size: 6vmin;
                text-shadow: 0 0 0.2ex <?php echo $palette->body_main; ?>;
            }
            main#content {
                display: flex;
                flex-flow: column;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title("Glax is best Dragon!", $render_args);

        $comm_types = commission_types();

        $open = false;
        foreach ( $comm_types as $type )
        {
            if ( $type->open )
            {
                $open = true;
                break;
            }
        }

        if ( $open )
        {
            echo mkelement(["p", ["style"=>"text-align: center; font-size: x-large;"], [
                "I'm open for commissions, check the ",
                new Link("/commission/", "commission page"),
                " for details."
            ]]);
        }


        echo '<div class="durg"></div>';
        echo mkelement(["p", [], [
            "Glax is a ", new Link("/cute/", "dragon"), " with ",
            new Link("/color/", "blue"), " ", new Link("/fluff/", "scales"), "."
        ]]);
    }
}

$page = new HomePage();
