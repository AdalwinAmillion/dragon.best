<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/gallery.php");
require_once(__dir__."/../lib/telegram.php");


class AttribInfo
{
    function __construct($set_name, $index_range, $meta)
    {
        $this->set_name = $set_name;
        if ( !is_array($index_range) )
            $index_range = [$index_range, $index_range];
        $this->index_min = $this->normalize_index($index_range[0]);
        $this->index_max = $this->normalize_index($index_range[1]);
        $this->meta = $meta;
    }

    function normalize_index($index)
    {
        if ( $index < 0 )
        {
            $set = Telegram::instance()->sticker_set($this->set_name);
            return sizeof($set->stickers) + $index;
        }
        return $index;
    }

    function match_image($image)
    {
        if ( $image->sticker->set_name != $this->set_name )
            return false;
        if ( $image->sticker_index < $this->index_min || $image->sticker_index > $this->index_max )
            return false;

        foreach ( $this->meta as $key => $value )
            $image->meta[$key] = $value;

        return true;
    }
}

class AttribBuilder
{
    function __construct($set_name)
    {
        $this->set_name = $set_name;
        $this->first_index = 0;
    }

    function add_info($count, $meta, $skip=1)
    {
        $first = $this->first_index;
        $last = $first + $count - 1;
        $this->first_index += $count + $skip;
        return new AttribInfo($this->set_name, [$first, $last] , $meta);
    }
}

class StickersPage extends DurgPage
{
    use CachedTelegramStickerGalleryTrait;

    public $title = "Stickers";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css"
    ];
    public $sets = [
        "GlaxDragon",
        "GlaxContrib",
        "FurNoises",
        "FurNoises2",
        "GlaxFriends",
        "UKDragons",
    ];
    public $scripts = [
        "/media/scripts/collapse.js",
        "https://twemoji.maxcdn.com/2/twemoji.min.js?2.4",
    ];
    public $pack_desc = [];

    function media_path()
    {
        return "/media/img/telegram/stickers";
    }

    function base_uri()
    {
        return "/stickers/";
    }

    protected function get_css_class($selected)
    {
        if ( $selected == 1 ) return "durgpic selected";
        if ( $selected == 2 ) return "bigdurg";
        return "durgpic";
    }

    function extra_head($render_args)
    {
        ?><style>
        .emoji {
            position: absolute;
            right: 5px;
            bottom: 5px;
            background: rgba(255, 255, 255, 0.5);
            padding: 2px;
        }
        .emoji .emoji-pic {
            width: 16px;
            height: auto;
        }
        .pic-preview {
            position: relative;
        }
        #emo-select {
            list-style: none;
            padding: 0;
            display: flex;
            font-size: 64px;
            flex-flow: row wrap;
            justify-content: center;
            margin: 0;
            user-select: none;
            -moz-user-select: none;
        }
        #emo-select > li {
            cursor: pointer;
            padding: 8px;
        }
        #emo-select > li:hover {
            background-color: #eee;
        }
        #emo-select > li.selected {
            border: 1px solid;
            padding: 7px;
            background-color: #ddd;
        }
        #emo-select .emoji-pic {
            width: 64px;
            height: auto;
        }
        h1[property="name"] {
            font-weight: normal;
            font-size: 64px;
        }
        .group-desc {
            text-align: center;
        }
        </style>
        <?php
    }

    function render_thumbnail_focused(MediaFileInfo $image, $render_args)
    {
        echo "<span class='pic-preview' data-emoji='{$image->title}'>";
        $image->render_thumbnail(
            $this->get_css_class(1),
            "",
            $this->thumbnail_meta($image)
        );
        echo mkelement(["span", ["class"=>"emoji"], $image->title]);
        echo "</span>";
    }

    function render_thumbnail($image, $render_args)
    {
        echo "<span class='pic-preview' data-emoji='{$image->title}'>";
        $image->render_thumbnail(
            $this->get_css_class(0),
            $image->details_url(),
            $this->thumbnail_meta($image)
        );
        echo mkelement(["span", ["class"=>"emoji"], $image->title]);
        echo "</span>";
    }

    protected function before_load()
    {
        $this->pack_desc = [
            "GlaxDragon" => [
                "My personal stickers, see also ",
                new Link("/vectors/", "vector art"),
                " for SVG sources and recoloring tools."
            ],
            "GlaxContrib" => [
                "Stickers of Glax not drawn by Glax."
            ],
            "FurNoises" => [
                "See ",
                new Link("/fur-noises/", "Fur Noises"),
                " if you want your sona to be included and for higher resolution images."
            ],
            "GlaxFriends" => [
                "Misc custom stickers Glax drew for his friends",
            ],
            "UKDragons" => [
                "Memes for the UK Dragons group.",
            ],
        ];
        $this->attribs = [
            new AttribInfo("GlaxContrib", [0, 120], ["author" => ""]),
            new AttribInfo("UKDragons", [0, 120], ["author" => ""]),
        ];

        /*$ab = new AttribBuilder("GlaxContrib");
        $this->attribs = [
            $ab->add_info(2, [
                "author" => "Seadragom",
                "copyrightYear" => "2017",
                "license" => null,
            ]),
            $ab->add_info(1, [
                "author" => "Weir",
                "copyrightYear" => "-",
                "license" => null,
                "extra" => [
                    "what" => "Base",
                    "author" => "Waitress",
                    "copyrightYear" => "2018",
                    "license" => null,
                ]
            ]),
            $ab->add_info(7, [
                "extra" => [
                    "what" => "Base",
                    "author" => "StupidSheperd",
                    "copyrightYear" => "2018",
                    "license" => null,
                ],
                "copyrightYear" => "-",
                "license" => null,
            ]),
            $ab->add_info(2, [
                "author" => "Sae-Fang",
                "copyrightYear" => "2018",
                "license" => null,
            ]),
            $ab->add_info(2, [
                "extra" => [
                    "what" => "Plushie",
                    "author" => "Sewpoke",
                    "copyrightYear" => "2018",
                    "license" => null,
                ],
                "copyrightYear" => "2018",
            ]),
            $ab->add_info(3, [
                "author" => "Wolfool",
                "copyrightYear" => "2018",
                "license" => null,
            ]),
            $ab->add_info(2, [
                "extra" => [
                    "what" => "Fursuit",
                    "author" => "Cabbagebath Furstuff",
                    "copyrightYear" => "2018",
                    "license" => null,
                ],
            ]),
            $ab->add_info(4, [
                "author" => "FelisRandomis",
                "copyrightYear" => "2018",
                "license" => null,
            ], 0),
            $ab->add_info(1, [
                "author" => "MCtoastie",
                "copyrightYear" => "2018",
                "license" => null,
            ]),
            $ab->add_info(1, [
                "author" => "Lunalei",
                "copyrightYear" => "2018",
                "license" => null,
            ]),
            $ab->add_info(2, [
                "author" => "Dracorum Order",
                "copyrightYear" => "2019",
                "license" => null,
            ], 0),
            $ab->add_info(1, [
                "author" => "Symrea",
                "copyrightYear" => "2019",
                "license" => null,
            ]),
            $ab->add_info(8, [
                "author" => "Royalty",
                "copyrightYear" => "2018",
                "license" => null,
            ], 0),
            $ab->add_info(33, [
                "author" => "StupidSheperd",
                "copyrightYear" => "2018",
                "license" => null,
            ], 0),
        ];*/
    }

    function humanmeta_template($image)
    {
        if ( $image->meta["author"] == "" )
            return "";

        if ( isset($image->meta["extra"]) )
            $extra = "<br/>%(extra.what:nometa) by %(extra.author) &copy; %(extra.copyrightYear) %(extra.license:html)";
        else
            $extra = "";

        if ( isset($image->meta["copyrightYear"]) && $image->meta["copyrightYear"] == "-" )
            $copy = "";
        else
            $copy = " &copy; %(copyrightYear)";

        return "Sticker by %(author)$copy %(license:html)$extra";
    }

    protected function load_image_metadata($image)
    {
        foreach ( $this->attribs as $attrib_info )
            if ( $attrib_info->match_image($image) )
                return;
    }

    function main($render_args)
    {
        $focus = $render_args["image"] ?? null;

        if ( $focus === null )
        {
            $this->render_filter($render_args);
        }

        $this->render_gallery($focus, $render_args);

        ?><script>
            document.querySelectorAll('h1[property="name"], .gallery').forEach(function(element){
                twemoji.parse(element, {
                    className: "emoji-pic",
                    attributes: function(rawText, iconId){
                        return {
                            "data-emoji-id": iconId.replace(/-.*/,""),
                        };
                    },
                });
            });

            var xhr = new XMLHttpRequest();
            xhr.overrideMimeType("application/json");
            xhr.open('GET', '/media/scripts/emoji-lookup.json', true);
            xhr.onreadystatechange = function ()
            {
                if (xhr.readyState == 4 && xhr.status == "200")
                {
                    var emoji_lookup = JSON.parse(xhr.responseText);
                    document.querySelectorAll("#emo-select .emoji-pic").forEach(function (element){
                        var emoji_name = emoji_lookup[element.getAttribute("data-emoji-id")];
                        if ( emoji_name )
                            element.setAttribute("title", emoji_name);
                        else
                        {
                            console.log(element.getAttribute("data-emoji-id")+" "+element.getAttribute("alt"));
                        }
                    });
                }
            };
            xhr.send(null);
        </script><?php
    }

    function render_group_title($group, $render_args)
    {
        echo mkelement([
            "h2",
            ["class"=>"gallery-title", "property" => "name"],
            $this->set_link($group->sticker_set)
        ]);
        if ( isset($this->pack_desc[$group->sticker_set->name]) )
            echo mkelement(["p", ["class"=>"group-desc"], $this->pack_desc[$group->sticker_set->name]]);
    }

    function extra_footer($render_args)
    {
        echo "<br/>";
        echo "Emoji icons by ",
            new Link("https://github.com/twitter/twemoji", "Twemoji"),
            " (",
            License::find_license("CC BY"),
            ")"
        ;
    }

    function render_filter($render_args)
    {
        $emojis = [];
        foreach ( $this->image_groups as $g )
            foreach ( $g->images as $img )
                array_push($emojis, $img->title);
        $emojis = array_unique($emojis);

        echo mkelement([
            "h2",
            ["class"=>"gallery-title", "data-collapse"=>"emo-select"],
            "Filter by Emoji"
        ]);
        echo "<ul id='emo-select'>";
        foreach ( $emojis as $emoji )
            echo mkelement(["li", ["data-emoji-select"=>$emoji], $emoji]);
        echo "</ul>";
        ?><script>
            var emo_select = document.getElementById("emo-select");
            var selected_li = null;
            function li_click()
            {
                var emoji = this.getAttribute("data-emoji-select");
                var attr_selector = "[data-emoji='" + emoji + "']"
                var not_matching = document.querySelectorAll("span[data-emoji]:not(" + attr_selector + ")");

                if ( selected_li !== null )
                    selected_li.classList.remove("selected");

                if ( selected_li === this )
                {
                    for ( var elem of not_matching )
                    {
                        elem.style.display = null;
                    }
                    selected_li = null;
                }
                else
                {
                    for ( var elem of not_matching )
                    {
                        elem.style.display = 'none';
                    }

                    if ( selected_li !== null )
                    {
                        var matching = document.querySelectorAll("span" + attr_selector);
                        for ( var elem of matching )
                        {
                            elem.style.display = 'inline';
                        }
                    }

                    selected_li = this;
                    this.classList.add("selected");
                }

            }

            for ( let li of emo_select.querySelectorAll("li") )
            {
                li.addEventListener("click", li_click.bind(li));
            }

            init_collapsables();
        </script><?php
    }
}

$page = new StickersPage();
