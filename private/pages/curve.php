<?php

require_once(__dir__."/../dragon.php");

class CurvePage extends DurgPage
{
    public $title = "Dragon Curve";
    public $description = "An interactive dragon curve visualizer.";
    public $default_image = "/media/img/icon/dragon_curve.png";
    public $scripts = [
        "/media/scripts/file_tools.js",
        "/media/scripts/curve.js",
    ];

    function extra_head($render_args)
    {
        ?>
        <style>
        #canvas {
            width: 100%;
            height: auto;
            display: block;
            margin: 0 auto;
            cursor: pointer;
        }
        .info-wrapper {
            white-space: nowrap;
        }
        </style>
        <?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
        global $palette;

        ?>

        <div>
            <button id="previous">Go up!</button>
            <button id="next">Go deeper!</button>
            <span class="info-wrapper">
                Depth
                <input type="number" min="0" max="0" value="0" id="depth" autocomplete="off"/>
            </span>
        </div>

        <p>Please be aware that the complexity of the dragon curve grows exponentially,
        so generating higher iterations might slow down your browser.</p>
        <p>Estimated time to generate the next iteration (level <span id="next_level">1</span>): <span id="estimate">0</span> seconds.</p>

        <svg
            htmlns="http://www.w3.org/2000/svg"
            id="canvas"
            title="Dragon curve"
            width="512"
            height="512"
            viewBox="0 0 512 512"
            style="stroke-width: 2px; stroke-linecap: square;"
        ></svg>
        <ul class="buttons">
            <li><a href="" onclick="event.preventDefault(); svg_element_save(canvas, 'dragon_curve_'+durg_curve.level+'.svg');">SVG</a></li>
            <li><a href="" onclick="event.preventDefault(); svg_element_save_png(canvas, 'dragon_curve_'+durg_curve.level+'.png', 512, 512);">PNG</a></li>
        </ul>


        <h2>Style</h2>
        <form autocomplete="off">
            <table>
                <tr>
                    <td><label for="stroke-width">Stoke width</label></td>
                    <td><input type="number" value="2" min="1" id="stroke-width"
                        onchange="canvas.style['stroke-width'] = this.value + 'px';"/>
                    </td>
                </tr>
                <tr>
                    <td><label for="stroke-cap">Stoke cap</label></td>
                    <td><select onchange="canvas.style['stroke-linecap'] = this.value;" id="stroke-cap"/>
                            <option value="butt">Butt</option>
                            <option value="round">Round</option>
                            <option value="square" selected="selected">Square</option>
                        </select>
                    </td>
                </tr>
            </table>

            <h3>Colors</h3>
            <div id="color-settings-parent">
            </div>
        </form>

        <script>
            var color_presets = {
                "Glax": [
                    Color.from_html('<?php echo $palette->belly->to_html(); ?>'),
                    Color.from_html('<?php echo $palette->body_main->to_html(); ?>'),
                    Color.from_html('<?php echo $palette->body_dark->to_html(); ?>'),
                ],
                "Fire": [
                    Color.from_html('#ffcc00'),
                    Color.from_html('#880000'),
                ],
                "Rainbow": [
                    Color.from_html('#ff0000'),
                    Color.from_html('#ffff00'),
                    Color.from_html('#00ff00'),
                    Color.from_html('#00ffff'),
                    Color.from_html('#0000ff'),
                    Color.from_html('#ff00ff'),
                ],
            }
            var color_selector = new ColorSelector(color_presets);

            var canvas = document.getElementById("canvas");
            var estimate = document.getElementById("estimate");
            var next_level = document.getElementById("next_level");
            var btn_next = document.getElementById("next");
            var btn_previous = document.getElementById("previous");
            var depth = document.getElementById("depth");

            var durg_curve = new DragonCurve(canvas);

            function _go_down_fast()
            {
                durg_curve.go_down();
                depth.max = durg_curve.levels.length - 1;
                depth.value = durg_curve.level;
                next_level.textContent = durg_curve.levels.length;
                canvas.setAttribute("title", "Dragon curve level " + durg_curve.level);
            }

            function _go_down_slow()
            {
                var t0 = performance.now();
                _go_down_fast();
                var delta = performance.now() - t0;

                if ( durg_curve.level == durg_curve.levels.length - 1 )
                {
                    btn_next.disabled = false;
                    btn_previous.disabled = false;
                    depth.disabled = false;
                    canvas.style.cursor = null;
                    var seconds = delta / 500;
                    // Add 2 to account for rendering time
                    if ( seconds > 0.6 )
                        seconds += 2;
                    estimate.textContent = seconds.toFixed(3);
                }
            }

            function go_down()
            {
                if ( durg_curve.level == durg_curve.levels.length - 1 )
                {
                    btn_next.disabled = true;
                    btn_previous.disabled = true;
                    depth.disabled = true;
                    canvas.style.cursor = "wait";
                    // small delay to give the browset the time to apply the style changes
                    window.setTimeout(_go_down_slow, 2);
                }
                else
                    _go_down_fast();
            }

            function go_up()
            {
                durg_curve.go_up();
                depth.value = durg_curve.level;
            }

            canvas.addEventListener("click", go_down);
            btn_next.addEventListener("click", go_down);
            btn_previous.addEventListener("click", go_up);
            depth.addEventListener("change", function(){ durg_curve.jump_to(Number(depth.value)); });

            color_selector.create_dom(document.getElementById("color-settings-parent"));
            color_selector.on_change = function(colors){
                durg_curve.update_gradient(colors);
            };
            color_selector.select_preset("Glax");
        </script>
        <?php
    }
};

$page = new CurvePage();

