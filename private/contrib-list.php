<?php

require_once(__dir__."/lib/pages/gallery.php");
require_once(__dir__."/lib/html.php");
require_once(__dir__."/models.php");

class Price
{
    function __construct($value, $currency)
    {
        $this->value = $value;
        $this->currency = $currency;
    }

    function __toString()
    {
        return "{$this->value} {$this->price}";
    }
}


class Source extends DisplayElement
{
    public static $icons = [
        "Furaffinity" => "fas fa-paw",
        "Website" => "fas fa-globe",
        "DeviantArt" => "fab fa-deviantart",
        "Telegram" => "fab fa-telegram-plane",
        "Twitter" => "fab fa-twitter",
        "Patreon" => "fab fa-patreon",
        "ArtFight" => "fas fa-shield-alt",
    ];

    function __construct($name, $uri, $is_discussion=true, $prefix=null)
    {
        $this->name = $name;
        $this->uri = $uri;
        $this->is_discussion = $is_discussion;
        $this->prefix = $prefix;
    }

    function element()
    {
        $link = PlainLink::nav_icon(
            $this->name,
            $this->uri,
            "navicon ". self::$icons[$this->name],
            $this->is_discussion ? ["property" => "discussionUrl"] : []
        );
        if ( $this->prefix )
            return new DisplayElementList([$this->prefix, $link]);
        return $link;
    }

    function render()
    {
        return $this->element()->render();
    }
}

class ContribArt
{
    const TYPE_IMAGE = 0;
    const TYPE_AUDIO = 1;
    const TYPE_VIDEO = 2;

    function __construct(
        $basename, $slug, $title, $attrs,
        Price $price=null, Source $source=null,
        $type=ContribArt::TYPE_IMAGE, $extra=null)
    {
        $this->basename = $basename;
        $this->slug = $slug;
        $this->title = $title;
        $this->attrs = $attrs;
        $this->price = $price;
        $this->source = $source;
        $this->type = $type;
        $this->extra = $extra;
    }

    function norm_price($currency="EUR")
    {
        if ( $this->price === null )
            return 0;

        if ( $currency == $this->price->currency )
            $rate = 1;
        else
            $rate = CurrencyConversion::get_rate($this->price->currency, $this->attrs[0]["dateCreated"]);
        return $this->price->value * $rate;
    }

    function to_media_info($page)
    {
        $meta = $this->attrs[0];
        if ( sizeof($this->attrs) > 1 )
            $meta["extra"] = $this->attrs[1];
        if ( $this->source !== null )
            $meta["source"] = $this->source;
        $meta["character"] = "Glax";
        if ( $this->type == self::TYPE_AUDIO )
        {
            return new AudioInfo(
                $page,
                $this->basename,
                $this->slug,
                $this->title,
                null,
                $meta
            );
        }
        if ( $this->type == self::TYPE_VIDEO )
        {
            return new VideoInfo(
                $page,
                $this->basename,
                $this->extra,
                $this->slug,
                $this->title,
                null,
                $meta
            );
        }
        return new ExplicitImageInfo(
            $page,
            $this->basename,
            $this->slug,
            $this->title,
            null,
            $meta
        );
    }
}

function contrib_art()
{
    return [
        new ContribArt(
            "tinafied.jpg",
            "tinafied",
            "Tinafied",
            [[
                "type" => "Gift",
                "author" => "Glax and Tina",
                "copyrightYear" => 2017,
                "dateCreated" => "2017-05-17",
            ]]
        ),
        new ContribArt(
            "thinking.png",
            "thinking",
            "Thinking Glax",
            [[
                "type" => "Commission",
                "author" => "Seadragom",
                "character" => "Glax",
                "copyrightYear" => 2017,
                "dateCreated" => "2017-10-28",
            ]],
            new Price(5, "GBP"),
            new Source(
                "Furaffinity",
                "http://www.furaffinity.net/view/25629964/",
                true
            )
        ),
        new ContribArt(
            "stick.jpg",
            "stick",
            "Stick",
            [[
                "type" => "Gift",
                "author" => "Arialana",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-01-04",
            ]]
        ),
        new ContribArt(
            "taming_of_a_dragon.jpg",
            "taming_of_a_dragon",
            "Taming of a dragon",
            [[
                "type" => "Commission",
                "author" => "Tochka",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-02-28",
            ]],
            new Price(50/2, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/26531510/",
                true,
                "Full image on "
            )

        ),
        new ContribArt(
            "sketch_by_symrea.jpg",
            "sketch_by_symrea",
            "Sketch",
            [[
                "type" => "Commission",
                "author" => "Symrea",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-03-08",
            ]],
            new Price(10, "GBP"),
            new Source(
                "Website",
                "https://adventofhope.wixsite.com/symrea/commissions",
                false
            )
        ),
        new ContribArt(
            "rollover.gif",
            "rollover",
            "Rolling over",
            [[
                "type" => "Commission",
                "author" => "Boardle",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-03-18",
            ]],
            new Price(135, "USD"),
            new Source(
                "DeviantArt",
                "https://www.deviantart.com/boardle/art/Commission-Glax-735157446",
                true
            )
        ),
        new ContribArt(
            "glax_by_gav.jpg",
            "glax_by_gav",
            "Sketch",
            [[
                "type" => "Gift",
                "author" => "Gav",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-05-02",
            ]],
            null,
            new Source(
                "Telegram",
                "https://t.me/Gvan2202"
            )
        ),
        new ContribArt(
            "glaxdrax.png",
            "glaxdrax",
            "Glax",
            [[
                "type" => "Trade",
                "author" => "Sae-Fang",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-05-05",
            ]]
        ),
        new ContribArt(
            "plushie.png",
            "plushie",
            "Mini Glax Plushie",
            [[
                "type" => "Commission",
                "author" => "Sewpoke",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-05-10",
            ]],
            new Price(155, "GBP"),
            new Source(
                "Twitter",
                "https://twitter.com/sewpoke_/status/994503122583252992",
                true
            )
        ),
        new ContribArt(
            "durgieweddingday.png",
            "weddingday",
            "Wedding day",
            [[
                "type" => "Gift",
                "author" => "Librase",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-05-14",
            ]]
        ),
        new ContribArt(
            "lpl.jpg",
            "lpl",
            "LPL",
            [[
                "type" => "Gift",
                "author" => "Librase and Katie",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-05-16",
            ]]
        ),
        new ContribArt(
            "badge_by_aroo.jpg",
            "badge_by_aroo",
            "Badge",
            [[
                "type" => "Gift",
                "author" => "Aroo",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-07-09",
            ]],
            null,
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/28166932/",
                true
            )
        ),
        new ContribArt(
            "wolfool_refsheet.png",
            "wolfool_refsheet",
            "Ref Sheet",
            [[
                "type" => "Commission",
                "author" => "Wolfool",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-08-01",
            ]],
            new Price(55, "GBP"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/28190771/",
                true
            )
        ),
        new ContribArt(
            "fursuit.jpg",
            "fursuit",
            "Fursuit",
            [[
                "type" => "Fursuit",
                "author" => "Cabbagebath Furstuff",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-08-11",
            ],[
                "type" => "Photo",
                "author" => "WolfStarHooves",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-08-25",
            ]],
            new Price(430, "GBP")
        ),
        new ContribArt(
            "color_sketch_maim.jpg",
            "color_sketch_maim",
            "Color Sketch",
            [[
                "type" => "Commission",
                "author" => "Maim",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-08-23",
            ]],
            new Price(45, "USD")
        ),
        new ContribArt(
            "moonlit_flight.jpg",
            "moonlit_flight",
            "Moonlit Flight",
            [[
                "type" => "Commission",
                "author" => "Shadow",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-09-11",
            ]],
            new Price(100, "GBP"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/28643455/",
                true
            )
        ),
        new ContribArt(
            "shep_stickers.png",
            "shep_stickers",
            "Stickers",
            [[
                "type" => "Commission",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-09-16",
            ]],
            new Price(60, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/28719382/",
                true
            )
        ),
        new ContribArt(
            "flop.png",
            "flop",
            "Flop",
            [[
                "type" => "Gift",
                "author" => "tib.234",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-09-16",
            ]],
            null,
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/28702177/",
                true
            )
        ),
        new ContribArt(
            "night_thought.jpg",
            "night_thought",
            "Night Thought",
            [[
                "type" => "Commission",
                "author" => "Shadow",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-09-18",
            ]],
            new Price(20, "GBP"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/28716066/",
                true
            )
        ),
        new ContribArt(
            "badge_by_booshie.png",
            "badge_by_booshie",
            "Badge",
            [[
                "type" => "Commission",
                "author" => "Booshie",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-09-25",
            ]],
            new Price(50, "GBP"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/28802149/",
                true
            )
        ),
        new ContribArt(
            "glax_commission_symrea.jpg",
            "glax_commission_symrea",
            "Commission",
            [[
                "type" => "Commission",
                "author" => "Symrea",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-10-01",
            ]],
            new Price(100, "GBP"),
            new Source(
                "Website",
                "https://adventofhope.wixsite.com/symrea/commissions"
            )
        ),
        new ContribArt(
            "stickers_felisrandomis.png",
            "stickers_felisrandomis",
            "Cheeky Glax",
            [[
                "type" => "Commission",
                "author" => "FelisRandomis",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-11-01",
            ]],
            new Price(64.95, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/29239342/",
                true
            )
        ),
        new ContribArt(
            "armour.ogv",
            "armour",
            "Armour",
            [[
                "type" => "Image",
                "author" => "LaDyStArK",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-11-06",
            ],[
                "type" => "Animation",
                "author" => "BlackAures",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-29",
            ]],
            new Price(200, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/29888988/",
                true
            ),
            ContribArt::TYPE_VIDEO,
            "armour.jpg"
        ),
        new ContribArt(
            "glax_by_lunalei.jpg",
            "glax_by_lunalei",
            "Cheeky Glax",
            [[
                "type" => "Commission",
                "author" => "Lunalei",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-11-07",
            ]],
            new Price(105, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/29301494/",
                true,
                "Full image on "
            )
        ),
        new ContribArt(
            "bronyscot.jpg",
            "bronyscot",
            "BronyScot",
            [[
                "type" => "(Paid for by BunnyPony) Commision",
                "author" => "Tina",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-11-09",
            ]],
            null,
            new Source(
                "DeviantArt",
                "https://www.deviantart.com/mirry92/art/Wip-771380811",
                true
            )
        ),
        /*
        TODO
        new ContribArt(
            "libs-sculpture.jpg",
            "libs-sculpture",
            "Glax",
            [[
                "type" => "Gift",
                "author" => "Librase",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-11-10",
            ]]
        ),*/
        new ContribArt(
            "hypnosis.ogg",
            "hypnosis",
            "Hypnosis",
            [[
                "type" => "Gift",
                "author" => "Princess Jinx",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-11-20",
            ]],
            null,
            null,
            ContribArt::TYPE_AUDIO
        ),
        new ContribArt(
            "bounce.gif",
            "bounce",
            "Bouncing Glax",
            [[
                "type" => "Commission",
                "author" => "FelisRandomis",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-11-21",
            ]],
            new Price(16.24, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/29453683/",
                true
            )
        ),
        new ContribArt(
            "grin.png",
            "grin",
            "Grinning Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-11-30",
            ]],
            new Price(10, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "ghosts.jpg",
            "ghosts",
            "Ghosts",
            [[
                "type" => "Commission",
                "author" => "LaDyStArK",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-02",
            ]],
            new Price(90.64, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/29580754/",
                true
            )
        ),
        new ContribArt(
            "glax-works.png",
            "works",
            "Not how it works",
            [[
                "type" => "Commission",
                "author" => "Seadragom",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-05",
            ]],
            new Price(6.21, "GBP")
        ),
        new ContribArt(
            "butt_hug.png",
            "butt_hug",
            "Butt hug",
            [[
                "type" => "Gift",
                "author" => "MCtoastie",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-11",
            ]]
        ),
        new ContribArt(
            "stride.gif",
            "stride",
            "Glax Stride",
            [[
                "type" => "Commission",
                "author" => "Boardle",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-20",
            ]],
            new Price(29, "USD"),
            new Source(
                "DeviantArt",
                "https://www.deviantart.com/boardle/art/Commission-YCH-Glax-777507394",
                true
            )
        ),
        new ContribArt(
            "bust_by_fefairy.png",
            "bust_by_fefairy",
            "Bust",
            [[
                "type" => "Commission",
                "author" => "Fefairy",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-22",
            ]],
            new Price(45, "EUR")
        ),
        new ContribArt(
            "last_ray.jpg",
            "last_ray",
            "Last Ray",
            [[
                "type" => "Commission",
                "author" => "LaDyStArK",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-23",
            ]],
            new Price(87.40, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/29840681/",
                true
            )
        ),
        new ContribArt(
            "santa.jpg",
            "santa",
            "Santa",
            [[
                "type" => "Gift",
                "author" => "Tina",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-23",
            ]]
        ),
        new ContribArt(
            "maw.jpg",
            "maw",
            "Maw",
            [[
                "type" => "Commission",
                "author" => "Symrea",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-27",
            ]],
            new Price(20, "GBP"),
            new Source(
                "Website",
                "https://adventofhope.wixsite.com/symrea/commissions",
                false
            )
        ),
        new ContribArt(
            "disgusted.png",
            "disgusted",
            "Disgusted Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2018,
                "dateCreated" => "2018-12-29",
            ]],
            new Price(15, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "dragon_nap.jpg",
            "dragon_nap",
            "Dragon's Nap",
            [[
                "type" => "Commission",
                "author" => "RHaenJarr",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-01-06",
            ]],
            new Price(377, "PLN"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/29994631/"
            )
        ),
        new ContribArt(
            "glax_by_kanevex.jpg",
            "glax_by_kanevex",
            "Glax Commission",
            [[
                "type" => "Commission",
                "author" => "Kanevex",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-01-13",
            ]],
            new Price(150, "EUR"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/30079594/"
            )
        ),
        new ContribArt(
            "distressed.png",
            "distressed",
            "Distressed Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-01-30",
            ]],
            new Price(18, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "dragonvelope.png",
            "dragonvelope",
            "Dragonvelope",
            [[
                "type" => "Gift",
                "author" => "Dracorum Order",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-02-09",
            ]]
        ),
        new ContribArt(
            "royalty_stickers.png",
            "royalty_stickers",
            "Glax Stickers",
            [[
                "type" => "Commission",
                "author" => "Royalty",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-02-12",
            ]],
            new Price(100, "USD")
        ),
        new ContribArt(
            "sad.png",
            "sad",
            "Sad Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-02-25",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "resting.jpg",
            "resting",
            "Resting in the mountains",
            [[
                "type" => "YCH",
                "author" => "Zazush-una",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-03-07",
            ]],
            new Price(280, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/30731537/"
            )
        ),
        new ContribArt(
            "my_light_will_dispel_darkness.jpg",
            "my_light_will_dispel_darkness",
            "My light will dispel darkness",
            [[
                "type" => "YCH",
                "author" => "LaDyStArK",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-03-11",
            ]],
            new Price(51.26, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/30778217/"
            )
        ),
        new ContribArt(
            "shep_stickers2.png",
            "shep_stickers2",
            "Stickers",
            [[
                "type" => "Commission",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-03-15",
            ]],
            new Price(110, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/31333337/"
            )
        ),
        new ContribArt(
            "take_flight.jpg",
            "take_flight",
            "Take Flight",
            [[
                "type" => "Commission",
                "author" => "Kitchiki",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-03-25",
            ]],
            new Price(100, "GBP"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/30945681/"
            )
        ),
        new ContribArt(
            "tired.png",
            "tired",
            "Tired Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-02-30",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "horny.png",
            "horny",
            "Horny Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-04-30",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "fishhorn.png",
            "fish-horn",
            "Fishy Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-06-04",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "lunalei-dance.jpg",
            "lunalei-dance",
            "A different kind of dance",
            [[
                "type" => "YCH",
                "author" => "Lunalei",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-06-11",
            ]],
            new Price(300, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/31930672/",
                true,
                "Full image on "
            )
        ),
        new ContribArt(
            "shep-flop.png",
            "shep-flop",
            "Flop",
            [[
                "type" => "Commission",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-06-13",
            ]],
            new Price(15, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/31985869/"
            )
        ),
        new ContribArt(
            "pride-pan.png",
            "pride-pan",
            "Pride month - Pan friends!",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-06-14",
            ]],
            new Price(0, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/posts/pride-month-pan-27623322"
            )
        ),
        new ContribArt(
            "reeva-ref.png",
            "reeva-ref",
            "Reeva Ref Sheet",
            [[
                "type" => "Commission",
                "author" => "Wolfool",
                "character" => "Reeva",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-06-30",
            ]],
            new Price(40, "GBP")
        ),
        new ContribArt(
            "think.png",
            "think",
            "Thinking Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-07-01",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "reeva-spread.jpg",
            "reeva-spread",
            "Reeva",
            [[
                "type" => "Commission",
                "author" => "Lunalei",
                "character" => "Reeva",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-08-02",
            ]],
            new Price(125, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/33163180/"
            )
        ),
        new ContribArt(
            "blub.png",
            "blub",
            "Blub",
            [[
                "type" => "Gift",
                "author" => "Clanks",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-08-06",
            ]]
        ),
        new ContribArt(
            "tophat.png",
            "tophat",
            "Glax with a top hat",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-08-17",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "reeva_by_whisperer.jpg",
            "reeva_by_whisperer",
            "Reeva by Whisperer",
            [[
                "type" => "YCH",
                "author" => "Whisperer",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-09-16",
            ]],
            new Price(110, "USD")/*,
            new Source(
                "Furaffinity",
                "",
                true,
                "Full image on "
            )*/
        ),
        new ContribArt(
            "pizza_party.png",
            "pizza_party",
            "Pizza Party",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-10-01",
            ]],
            null,
            new Source(
                "Patreon",
                "https://www.patreon.com/posts/pizza-party-30393854"
            )
        ),
        new ContribArt(
            "evil-laughter.png",
            "evil-laughter",
            "Glax's evil laughter",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-10-03",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "glax_and_reeva.jpg",
            "glax_and_reeva",
            "Glax and Reeva",
            [[
                "type" => "Commission",
                "author" => "Lunalei",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-10-26",
            ]],
            new Price(410, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/33739178/",
                true,
                "Full image on "
            )
        ),
        new ContribArt(
            "lilmew.jpg",
            "lilmew",
            "Glax",
            [[
                "type" => "Gift",
                "author" => "Lil Mew",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-11-03",
            ]]
        ),
        new ContribArt(
            "crying.png",
            "crying",
            "Crying Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-11-27",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "finger_guns.png",
            "finger_guns",
            "Glax with Finger Guns",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-11-28",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "in-the-sun.png",
            "in-the-sun",
            "Glax in the sun",
            [[
                "type" => "Commission",
                "author" => "LaDyStArK",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-12-06",
            ]],
            new Price(80, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/34122504/"
            )
        ),
        new ContribArt(
            "clanks-blep.png",
            "clanks-blep",
            "Blep",
            [[
                "type" => "Gift",
                "author" => "Clanks",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-12-08",
            ]]
        ),
        new ContribArt(
            "pot-holders.jpg",
            "pot-holders",
            "Glax Pot Holders",
            [[
                "type" => "Gift",
                "author" => "my mum",
                "character" => "Glax",
                "copyrightYear" => 2019,
                "dateCreated" => "2019-12-25",
            ]]
        ),
        new ContribArt(
            "pepper.png",
            "pepper",
            "Glax eating a pepper",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-01-03",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/Stupidshepherd"
            )
        ),
        new ContribArt(
            "nfc-sketch.jpg",
            "nfc-sketch",
            "Fursuit sketch",
            [[
                "type" => "Gift",
                "author" => "Doggett",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-02-18",
            ]]
        ),
        new ContribArt(
            "dragon-party.jpg",
            "dragon-party",
            "Dragon Party",
            [[
                "type" => "Commissin",
                "author" => "Shinigamisquirrel",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-03-02",
            ]],
            new Price(35, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/35252087/",
                true,
                "Full image on "
            )
        ),
        new ContribArt(
            "mouse.png",
            "mouse",
            "Mouse Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-03-16",
            ]],
            null,
            new Source(
                "Patreon",
                "https://www.patreon.com/posts/stream-freebies-34930124"
            )
        ),
        new ContribArt(
            "oxlwow.png",
            "oxlwow",
            "Oxl Glax",
            [[
                "type" => "Trade",
                "author" => "DirtyPaws",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-02-13",
            ]],
            null,
            null
        ),
        new ContribArt(
            "peek.png",
            "peek",
            "Peeking Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-04-01",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/posts/stream-freebies-34930124"
            )
        ),
        new ContribArt(
            "shush.png",
            "shush",
            "Shushing Glax",
            [[
                "type" => "Patreon reward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-04-01",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/posts/stream-freebies-34930124"
            )
        ),
        new ContribArt(
            "tail-scarf.jpg",
            "tail-scarf",
            "Tail Scarf",
            [[
                "type" => "Gift",
                "author" => "Allon",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-04-01",
            ],[
                "type" => "Art",
                "author" => "Impbutt",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-04-01",
            ],]
        ),
        new ContribArt(
            "malik_spoon.png",
            "malik_spoon",
            "Glax spooning Malik",
            [[
                "type" => "Commission",
                "author" => "NitroDS",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-05-16",
            ]],
            new Price(180, "EUR"),
            null
//             new Source(
//                 "Furaffinity",
//                 "https://www.furaffinity.net/view/",
//                 true,
//                 "Full image on "
//             )
        ),
        new ContribArt(
            "swimming.jpg",
            "swimming",
            "Glax swimming with CALL and Oda",
            [[
                "type" => "Patreon reward",
                "author" => "Herpydragon",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-06-17",
            ]],
            new Price(30, "EUR"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/36842506/",
                true,
                "Full image on "
            )
        ),
        new ContribArt(
            "tina-sleep.png",
            "tina-sleep",
            "Glax sleeping on a keyboard",
            [[
                "type" => "Charity stream",
                "author" => "Tina Fountain Heart",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-06-26",
            ]],
            new Price(5, "EUR"),
            new Source(
                "DeviantArt",
                "https://www.deviantart.com/mirry92"
            )
        ),
        new ContribArt(
            "meeesh-attack.png",
            "meeesh-attack",
            "Artfight Attack",
            [[
                "type" => "ArtFight attack",
                "author" => "Meeesh",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-07-02",
            ]],
            null,
            new Source(
                "ArtFight",
                "https://artfight.net/attack/1028177.glax"
            )
        ),
        new ContribArt(
            "in-the-tavern.png",
            "in-the-tavern",
            "In the Tavern",
            [[
                "type" => "YCH Auction",
                "author" => "JArts",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-07-02",
            ]],
            new Price(50, "USD"),
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/37089563/"
            )
        ),
        new ContribArt(
            "nicolas-attack.png",
            "nicolas-attack",
            "Artfight Attack",
            [[
                "type" => "ArtFight attack",
                "author" => "Nicolas",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-07-10",
            ]],
            null,
            new Source(
                "ArtFight",
                "https://artfight.net/attack/1308119.glax-3"
            )
        ),
        new ContribArt(
            "yahweh.png",
            "yahweh",
            "God Glax",
            [[
                "type" => "Drawn",
                "author" => "Vampi",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-07-23",
            ],[
                "type" => "Trade",
                "author" => "IceDragon Northstar",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-07-23",
            ]],
            null,
            null
        ),
        new ContribArt(
            "knitted.jpg",
            "knitted",
            "Knitted Baby Glax",
            [[
                "type" => "Gift",
                "author" => "My Mum",
                "character" => "Glax",
                "copyrightYear" => 2020,
                "dateCreated" => "2020-11-26",
            ]],
            null,
            null
        ),
        new ContribArt(
            "christmas.jpg",
            "christmas",
            "Christmas Group Pic",
            [[
                "type" => "Patreon rward",
                "author" => "StupidShepherd",
                "character" => "Glax",
                "copyrightYear" => 2021,
                "dateCreated" => "2021-01-03",
            ]],
            new Price(12, "USD"),
            new Source(
                "Patreon",
                "https://www.patreon.com/posts/group-pic-2020-45749905",
                true,
                "Full image on "
            )
        ),
        new ContribArt(
            "ribbon.jpg",
            "ribbon",
            "Ribbon",
            [[
                "type" => "Commission",
                "author" => "PalDreamer",
                "character" => "Glax",
                "copyrightYear" => 2021,
                "dateCreated" => "2021-02-15",
            ]],
            null,
            new Source(
                "Furaffinity",
                "https://www.furaffinity.net/view/40678131/"
            )
        ),
    ];
}
