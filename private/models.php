<?php

require_once(__dir__."/lib/db/model.php");
require_once(__dir__."/init.php");

class DragonLocation extends Model
{
    # Phinx needs defaults to make columns not null
    protected $telegram_id = ["string", "default"=>"", "limit"=>16, "unique"=>true];
    protected $dragon = ["boolean", "default"=>true];
    protected $public = ["boolean", "default"=>true];
    protected $shadowed = ["boolean", "default"=>false];
    protected $lat = ["decimal", "default"=>0];
    protected $lon = ["decimal", "default"=>0];
    protected $icon_url = ["string", "limit"=>128, "default"=>""];
}

class CurrencyConversion extends Model
{
    # $from * $rate = $to
    protected $date = ["date"];
    protected $from = ["string", "limit"=>3];
    protected $to = ["string", "limit"=>3, "default"=>"EUR"];
    protected $rate = ["decimal"];

    static function get_rate($from, $date, $to="EUR")
    {
        $obj = static::query()
            ->where("from", "=", $from)
            ->where("date", "=", $date)
            ->where("to", "=", $to)
            ->first()
        ;
        if ( $obj === null )
            $obj = static::query_api($from, $date, $to);

        if ( $obj === null )
            return -1;

        return $obj->data["rate"];
    }

    static function query_api($from, $date, $to="EUR")
    {
        global $site;
        $url = "http://data.fixer.io/api/$date?" . http_build_query([
            "access_key" => $site->settings->fixer_api_key,
            "base" => $to,
            "symbols" => $from
        ]);
        $api_rez = json_decode(file_get_contents($url), true);
        if ( !$api_rez )
        {
            user_error("Fixer: could not open $url");
            return null;
        }
        else if ( !$api_rez["success"] )
        {
            user_error("Fixer: {$api_rez['error']['code']} ({$api_rez['error']['type']}): {$api_rez['error']['info']}\n$url");
            return null;
        }
        $obj = new CurrencyConversion([
            "date" => $date,
            "from" => $from,
            "to" => $to,
            "rate" => 1.0/$api_rez["rates"][$from],
        ]);
        $obj->save();
        return $obj;
    }
}

class DragonSizeType
{
    function __construct($name, $height, $height_mult, $length, $length_mult, $img_wh, $x_offset)
    {
        $this->name = $name;
        $this->height = $height;
        $this->height_mult = $height_mult;
        $this->length = $length;
        $this->length_mult = $length_mult;
        $this->img_wh = $img_wh;
        $this->x_offset = $x_offset;
    }
}

class DragonSize extends Model
{
    public static $types = [];
    protected $telegram_id = ["string", "default"=>"", "limit"=>16];
    protected $type = ["string", "default"=>"", "limit"=>8];
    protected $icon_url = ["string", "limit"=>128, "default"=>""];
    protected $name = ["string", "limit"=>128, "default"=>""];
    protected $height = ["decimal", "default"=>0];
    protected $length = ["decimal", "default"=>0];
    protected $color = ["string", "default"=>"#cccccc", "limit"=>7];
}



DragonSize::$types = [
    "derg" =>   new DragonSizeType("Feral", "Shoulder height",  2, "Length", 1, 640/480, 0.90),
    "wingless"=>new DragonSizeType("Wingless","Shoulder height",2, "Length", 1, 640/480, 0.60),
    "noodle" => new DragonSizeType("Noodle", "",              4.5, "Length", 1, 822/217, 0.30),
    "anthro" => new DragonSizeType("Anthro", "Height",          1, "",       3, 434/512, 0.90),
    "kobold" => new DragonSizeType("Kobold", "Height",          1, "",       3, 434/512, 0.80),
];

global $site;
$site->connect_db();
