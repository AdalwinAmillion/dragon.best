<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<?php
    $sizes = [
        "16x16" => "16.png",
        "24x24" => "24.png",
        "32x32" => "32.png",
    ];
//         "any" => ["scalable.svg", "image/svg+xml"]
    foreach ( $sizes as $size => $file )
        echo mkelement(["link", [
            "rel" => "icon",
            "href" => href("/media/img/icon/$file"),
            "sizes" => $size,
            "type" => "image/png",
        ]]);
?>

<title><?php echo escape($this->title($render_args)); ?></title>

<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" />
<?php
    $ogmeta = $this->opengraph_metadata($render_args);
    $show_twitter = false;

    if ( $ogmeta["type"] == "photo" )
    {
        $show_twitter = true;
        echo '<meta name="twitter:card" content="summary_large_image" />';
    }

    foreach ( $ogmeta as $type => $content )
    {
        echo mkelement(["meta", ["property"=>"og:$type", "content" => $content]]);

        if ( $type == "description" )
            echo mkelement(["meta", ["name"=>$type, "content"=>$content]]);

        if ( $show_twitter && in_array($type, ["image", "title", "description", "url"]) )
            echo mkelement(["meta", ["property"=>"twitter:$type", "content" => $content]]);

        echo "\n";
    }
    foreach ( $this->styles as $styles )
    {
        echo mkelement([
            "link",
            array(
                "rel" => "stylesheet",
                "type" => "text/css",
                "href" => href($styles)
            )
        ]);
    }

    foreach ( $this->scripts as $script )
    {
        echo mkelement([
            "script",
            array("src" => href($script)),
            ""
        ]);
    }

    $this->extra_head($render_args);
