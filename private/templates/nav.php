<nav class="menu">
<?php
    $navlinks = [
        new SimpleElement("li", ["class"=>"logo"],
            new PlainLink("/", "",  ["title"=>"Glax is best dragon"])
        ),
        new Menu("Fun", [
            Link::nav_icon("Rawr!",                 "/rawr/",       "navicon fa-fw fas fa-bullhorn"),
            Link::nav_icon("Durg Time",             "/time/",       "navicon fa-fw far fa-clock"),
            Link::nav_icon("Dragon Curve",          "/curve/",      "navicon fa-fw fas fa-chart-line"),
            Link::nav_icon("Dragon Test",           "/test/",       "navicon fa-fw far fa-question-circle"),
            Link::nav_icon("Are dragons awesome?",  "/awesome/",    "navicon fa-fw fas fa-dragon"),
            Link::nav_icon("Angle Dragons",         "/angle/",      "navicon fa-fw fas fa-shapes"),
            Link::nav_icon("API",                   "/api/",        "navicon fa-fw fas fa-code"),
            Link::nav_icon("Telegram",              "/telegram/",   "navicon fa-fw fab fa-telegram-plane"),
            Link::nav_icon("Weather",               "/weather/",    "navicon fa-fw fas fa-cloud-sun-rain"),
            Link::nav_icon("Cookie",                "/cookie/",     "navicon fa-fw fas fa-cookie-bite"),
            Link::nav_icon("Tongue",                "/tongue/",     "navicon fa-fw fas fa-grin-tongue-squint"),
            Link::nav_icon("Captcha",               "/captcha/",    "navicon fa-fw fas fa-robot"),
        ]),
        new Menu("Art", [
            Link::nav_icon("Commission",            "/commission/", "navicon fa-fw fas fa-money-bill-wave"),
            Link::nav_icon("Vectors",               "/vectors/",    "navicon fa-fw fas fa-bezier-curve"),
            Link::nav_icon("Lottie Animations",     "/lottie/",     "navicon fa-fw fas fa-video"),
            Link::nav_icon("Sticker Editor",        "/editor/",     "navicon fa-fw fas fa-pencil-alt"),
            Link::nav_icon("Fur Noises",            "/fur-noises/", "navicon fa-fw fas fa-bullhorn"),
            Link::nav_icon("Telegram Stickers",     "/stickers/",   "navicon fa-fw fab fa-telegram"),
            Link::nav_icon("Ascii Art",             "/ascii/",      "navicon fa-fw fas fa-font"),
            Link::nav_icon("Gifs",                  "/gifs/",       "navicon fa-fw fas fa-video"),
            Link::nav_icon("Refs",                  "/refs/",       "navicon fa-fw fas fa-dragon"),
            Link::nav_icon("Fursuiting",            "/fursuit/",    "navicon fa-fw fas fa-mask"),
            Link::nav_icon("Contrib",               "/contrib/",    "navicon fa-fw far fa-images"),
        ]),
        new Menu("Dragons", [
            Link::nav_icon("List",                      "/dragons/","navicon fa-fw fas fa-list", [], false),
            Link::nav_icon("Points",             "/dragons/points/","navicon fa-fw fas fa-coins"),
            Link::nav_icon("Map",                   "/dragons/map/","navicon fa-fw fas fa-map-marked"),
            Link::nav_icon("Size Chart",           "/dragons/size/","navicon fa-fw fas fa-ruler-combined"),
        ]),
        "Contact" => "/contact/",
        new SimpleElement("li", ["class"=>"separator"], ""),
    ];


    global $auth;

    if ( $auth->user )
    {
        $usermenu = new Menu(
            mkelement(
                ["span", [], [$auth->user->display_name]],
                ["img", ["src"=>$auth->user->photo_url, "class"=>"profile-pic"]]
            ), [
                Link::nav_icon("Logout", "/auth/logout/?next={$_SERVER['REQUEST_URI']}", "navicon fa-fw fas fa-sign-out-alt")
            ]
        );

        if ( $auth->is_admin() )
        {
            global $site;
            foreach ( $site->settings->alt_uris as $name => $prefix )
            {
                $usermenu->link_list->add(Link::nav_icon(
                    $name,
                    $prefix.$_SERVER["REQUEST_URI"],
                    "navicon fa-fw fas fa-globe"
                ));
            }
        }

        $navlinks[] = $usermenu;
    }
    else
    {
        $navlinks["Login"] = new PlainLink(
            "/auth/login/?next={$_SERVER['REQUEST_URI']}",
            "Login",
            ["rel" => "nofollow"]
        );
    }

    echo new LinkList($navlinks);
?>
</nav>
