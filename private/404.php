<?php

require_once(__dir__."/dragon.php");
require_once(__dir__."/lib/pages/error.php");

class NotFound extends DurgPage
{
    use NotFoundTrait;
    public $title = "Durg Not Found";

    function main($render_args)
    {
        echo mkelement(
            ["h1", [], ["The page {$render_args["path"]} was not found in the dragon's hoard"]],
            ["img", [
                "src"=>href("/media/img/vectors/bored.svg"),
                "alt"=>"a dragon staring at the screen",
                "style"=>"max-width: 512px; width:100%;"
            ]]
        );
    }
};

$page = new NotFound();

