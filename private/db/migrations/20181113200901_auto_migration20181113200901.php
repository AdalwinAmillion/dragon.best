<?php


use Phinx\Migration\AbstractMigration;
require_once(__dir__."/../../lib/db/migration.php");

class AutoMigration20181113200901 extends BaseMigration
{
    /**
     * Change Models Method.
     *
     * Write your reversible migrations using this method.
     *
     * The following commands can be used in this method and the migration will
     * automatically reverse them when rolling back:
     *
     *    create_model
     *    add_field
     *    rename_field
     *    alter_field
     *    remove_field
     *    remove_model
     *
     */
    public function change_models()
    {
        
        $this->create_model('DragonLocation',
            [
                'table_name' => 'dragon_location',
            ],
            [
                'telegram_id' => ['string','column_name'=>'telegram_id','column_type'=>'string','default'=>'','has_column'=>true,'limit'=>16,'null'=>false,'unique'=>true,],
                'dragon' => ['boolean','column_name'=>'dragon','column_type'=>'boolean','default'=>true,'has_column'=>true,'null'=>false,],
                'public' => ['boolean','column_name'=>'public','column_type'=>'boolean','default'=>true,'has_column'=>true,'null'=>false,],
                'shadowed' => ['boolean','column_name'=>'shadowed','column_type'=>'boolean','default'=>false,'has_column'=>true,'null'=>false,],
                'lat' => ['decimal','column_name'=>'lat','column_type'=>'decimal','default'=>0,'has_column'=>true,'null'=>false,],
                'lon' => ['decimal','column_name'=>'lon','column_type'=>'decimal','default'=>0,'has_column'=>true,'null'=>false,],
                'icon_url' => ['string','column_name'=>'icon_url','column_type'=>'string','default'=>'','has_column'=>true,'limit'=>64,'null'=>false,],
                'id' => ['integer','column_name'=>'id','column_type'=>'integer','default'=>0,'has_column'=>true,'identity'=>true,'null'=>false,],
            ]
        );
    }
}
