// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPLv3+
/**
 * \brief Triggers a download on on the client side
 */
function save_url(url, filename)
{
    var link = document.createElement("a");
    link.href = url;
    link.download = filename;
    link.style.display = "none";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

/**
 * \brief Triggers a download of the given File object
 */
function save_file(file, name=null)
{
    if ( name === null )
        name = file.name;

    var url = URL.createObjectURL(file);
    save_url(url, file.name);
    URL.revokeObjectURL(url);
}

/**
 * \brief Returns a File object from a SVG document DOM element
 */
function svg_element_to_file(svgdom, filename)
{
    var serializer = new XMLSerializer();
    var content = serializer.serializeToString(svgdom);
    return new File([content], filename, {type:"image/svg+xml"});
}

/**
 * \brief Triggers a download for the SVG described in the DOM
 */
function svg_element_save(svgdom, filename)
{
    var file = svg_element_to_file(svgdom, filename);
    save_file(file);
}

/**
 * \brief Create a data: url from the given SVG rendered as PNG
 * \param svgdom    SVG element to export
 * \param width     Output width in pixels
 * \param height    Output height in pixels
 * \param callback  Called upon completion. It gets passed a data url and file name.
 */
function svg_element_to_png(svgdom, filename, width, height, callback)
{
    var file = svg_element_to_file(svgdom, filename);
    var url = URL.createObjectURL(file);
    var canvas = document.createElement("canvas");
    canvas.width = width || svgdom.width;
    canvas.height = height || svgdom.height;
    var ctx = canvas.getContext("2d");
    var img = document.createElement("img");
    img.width = svgdom.width;
    img.height = svgdom.height;
    img.setAttribute("src", url);
    img.onload = function() {
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        var data_url = canvas.toDataURL("image/png");
        callback(data_url, filename);
        URL.revokeObjectURL(url, filename);
    };
}

/**
 * \brief Render the SVG and triggers a download
 */
function svg_element_save_png(svgdom, filename, width, height)
{
    svg_element_to_png(
        svgdom, filename, width, height,
        function(data_url, filename){
            save_url(data_url, filename);
        }
    )
}
