// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPLv3+

/**
 * \brief Class for specifying how color changes are applied
 */
class RecolorRule
{
    /**
     * \param name           Machine-readable name of the color (for inputs and such)
     * \param label          Human-readable name of the color
     * \param original_color Starting color in `rgb()` notation (to extract it from the DOM)
     * \param current_color  Current color in #rrggbb notation (for input value)
     */
    constructor(name, label, original_color)
    {
        this.name = name;
        this.label = label;
        this.original_color = original_color;
        this.stroke_elements = [];
        this.fill_elements = [];
        var matches = RecolorRule.rgbregex.exec(original_color);
        if ( !matches )
        {
            console.log("Invalid color: " + original_color);
            matches = [0, 0, 0];
        }
        this.original_hex = '#' + this.to_hex(matches[1]) + this.to_hex(matches[2]) + this.to_hex(matches[3]);
        this.current_color = this.original_hex;
    }
    /**
     * \brief Convert color component to a 2 digit hex
     */
    to_hex(dec)
    {
        return ("0" + Number(dec).toString(16)).slice(-2);
    }

    append(element, content=null)
    {
        if ( !element.style )
            return RecolorRule.RULE_NONE;

        if ( content === null )
            content = element;

        var affects = RecolorRule.RULE_NONE;

        if ( this.original_color == element.style.stroke )
        {
            this.append_stroke(content);
            affects |= RecolorRule.RULE_STROKE;
        }

        if ( this.original_color == element.style.fill )
        {
            this.append_fill(content);
            affects |= RecolorRule.RULE_FILL;
        }

        return affects;
    }

    append_stroke(element)
    {
        this.stroke_elements = this.stroke_elements.concat(element);
    }

    append_fill(element)
    {
        this.fill_elements = this.fill_elements.concat(element);
    }

    has_elements()
    {
        return this.fill_elements.length || this.stroke_elements.length;
    }

    recolor(color)
    {
        this.current_color = color;

        for ( let element of this.stroke_elements )
            element.style.stroke = color;

        for ( let element of this.fill_elements )
            element.style.fill = color;
    }
}

RecolorRule.rgbregex = /rgba?\(([0-9]+)\s*,\s*([0-9]+)\s*,\s*([0-9]+)\s*(,\s*([0-9]+)\s*)?\)/i;
RecolorRule.RULE_NONE = 0;
RecolorRule.RULE_FILL = 1;
RecolorRule.RULE_STROKE = 2;

function is_visible(element)
{
    return element.getClientRects().length > 0;
}

class RecolorController
{
    constructor(handle_query_string, advanced_rules=false, table_layout=false, on_change=null)
    {
        this.handle_query_string = handle_query_string;
        this.advanced_rules = advanced_rules;
        this.table_layout = table_layout;
        this.on_change = on_change;

        /**
         * \brief Defines the rules used to change colors in the SVGs
         * \note Dictionary color_name => RecolorRule
         */
        this.recolor_rules = {};

        /**
        * \brief Color state map
        * \note Dictionary color_name => #rrggbb, show_`feature_name` => value
        *
        * Pushed as history state so all the elements are recolored when navigating
        * through the page history
        */
        this.recolor_state = {};

        this.feature_switchers = {};


        if ( this.handle_query_string )
        {
            /// URL query as an object
            this.get_params = parse_query_string();
        }
        else
        {
            this.get_params = {};
        }
    }

    append_feature_element(feature, value, element)
    {
        var switcher = this.feature_switchers[feature];

        if ( switcher === undefined )
        {
            switcher = this.feature_switchers[feature] = new FeatureSwitcher("show_" + feature, slug2title(feature));
            this.recolor_state[switcher.name] = switcher.default_value;
        }

        if ( switcher.default_value == "none" && element.style && element.style.display != "none" )
        {
            switcher.default_value = value;
            switcher.active = value;
            this.recolor_state[switcher.name] = value;
        }

        switcher.append(value, element);
    }

    append_rule_element(color_rgb, element)
    {
        var rule = this.append_rule(color_rgb);
        if ( !rule )
            return RecolorRule.RULE_NONE;
        return rule.append(element);
    }

    append_rule(color_rgb, name=null, label=null)
    {
        if ( !color_rgb.startsWith("rgb") )
            return null;

        var color_number = Object.keys(this.recolor_rules).length;
        var rule_name = name ? name : "color_" + color_number;
        var rule_label = label ? label : "Color " + color_number;
        var new_rule = new RecolorRule(
            rule_name,
            rule_label,
            color_rgb
        );
        this.recolor_rules[new_rule.name] = new_rule;
        this.recolor_state[new_rule.name] = new_rule.original_hex;
        if ( this.get_params[new_rule.name] )
            new_rule.current_color = this.get_params[new_rule.name];
        return new_rule;
    }

    /**
     * \brief Initialize automatic rules, every color in the SVG will be grouped together
     */
    init_rules_based_on_colors(svgdom)
    {
        // CSS rules
        for ( var stylesheet of svgdom.styleSheets )
        {
            for ( var rule of stylesheet.cssRules )
            {
                var elements = Array.from(svgdom.querySelectorAll(rule.selectorText)).filter(is_visible);

                for ( let recolor of Object.values(this.recolor_rules) )
                {
                    recolor.append(rule, elements);
                }
            }
        }
        // Inline style
        for ( var element of svgdom.getElementsByTagName("*") )
        {
            if ( !is_visible(element) )
                continue;

            var affects = RecolorRule.RULE_NONE;
            for ( let recolor of Object.values(this.recolor_rules) )
            {
                affects |= recolor.append(element);
            }

            if ( !(affects & RecolorRule.RULE_FILL) && element.style.fill && element.style.fill != "none" )
            {
                affects |= this.append_rule_element(element.style.fill, element);
            }

            if ( !(affects & RecolorRule.RULE_STROKE) && element.style.stroke && element.style.stroke != "none" )
            {
                affects |= this.append_rule_element(element.style.stroke, element);
            }

            if ( !(affects & RecolorRule.RULE_FILL) )
            {
                if ( element.style && element.tagName != "svg" && element.tagName != "g" )
                    element.style["pointer-events"] = 'none';
            }
        }
    }

    /**
     * \brief Initialize advanced rules, using data-fill and data-outline attributes
     */
    init_rules_based_on_data(svgdom)
    {
        for ( var element of svgdom.getElementsByTagName("*") )
        {
            var fill = element.getAttribute("data-fill");
            if ( fill )
            {
                var rule = this.recolor_rules[fill];
                if ( !rule )
                    rule = this.append_rule(element.style.fill, fill, slug2title(fill));

                if ( rule )
                    rule.append_fill(element);
            }
            else
            {
                if ( element.style && element.tagName != "svg" && element.tagName != "g" )
                    element.style["pointer-events"] = 'none';
            }


            var stroke = element.getAttribute("data-outline");
            if ( stroke )
            {
                var rule = this.recolor_rules[stroke];
                if ( !rule )
                    rule = this.append_rule(element.style.stroke, stroke, slug2title(stroke));

                if ( rule )
                    rule.append_stroke(element);
            }

            var toggle_group = element.getAttribute("data-toggle-group");
            var toggle = element.getAttribute("data-toggle");
            if ( toggle && toggle_group )
            {
                for ( var value of toggle.split(";") )
                    this.append_feature_element(toggle_group, value, element);
            }
        }
    }

    /**
     * \brief Initialize the color rules from the given SVG DomDocument
     */
    init_rules(svgdom)
    {
        if ( this.advanced_rules )
            this.init_rules_based_on_data(svgdom);
        else
            this.init_rules_based_on_colors(svgdom);
    }

    /**
     * \brief Sets up an iframe pointing to a SVG file, linking elements to their RecolorRule
     * \param frame DOM element for the iframe
     */
    init_frame(frame)
    {
        var svgdom = frame.contentDocument;
        this.init_rules(svgdom);

        // Enforce cropping outside the viewbox
        var par = svgdom.documentElement.preserveAspectRatio.baseVal;
        par.align = SVGPreserveAspectRatio.SVG_PRESERVEASPECTRATIO_XMIDYMID;
        par.meetOrSlice = SVGPreserveAspectRatio.SVG_MEETORSLICE_SLICE;
        var box = svgdom.documentElement.viewBox.baseVal;
        var wh_ratio = box.width / box.height;
        frame.style.height = frame.offsetWidth / wh_ratio + 'px';
        svgdom.documentElement.style.width = "100%";
        svgdom.documentElement.style.height = "100%";
    }

    /**
     * \brief Restores initial colors
     */
    reset_colors()
    {
        for ( let rule of Object.values(this.recolor_rules) )
        {
            this.recolor_state[rule.name] = rule.original_hex;
            if ( rule.original_hex != rule.current_color )
                rule.recolor(rule.original_hex);
        }

        for ( let rule of Object.values(this.feature_switchers) )
        {
            this.recolor_state[rule.name] = rule.default_value;
            rule.activate(rule.default_value);
        }

        if ( this.handle_query_string )
        {
            var uri = window.location.href.replace(/\?.*/,"");
            window.history.pushState(this.recolor_state, "", uri);
        }
    }

    randomize_colors()
    {
        function rand_channel()
        {
            return Math.round(Math.random()*255).toString(16).padStart(2, "0");
        }

        for ( let rule of Object.values(this.recolor_rules) )
        {
            var random_hex = "#"+rand_channel()+rand_channel()+rand_channel();
            this.recolor_state[rule.name] = random_hex;
            rule.recolor(random_hex);
        }

        for ( let rule of Object.values(this.feature_switchers) )
        {
            var choices = Object.keys(rule.features);
            var choice = choices[Math.floor(Math.random()*Object.keys(choices).length)];
            this.recolor_state[rule.name] = choice;
            rule.activate(choice);
        }

        if ( this.handle_query_string )
        {
            var uri = window.location.href.replace(/\?.*/,"");
            window.history.pushState(this.recolor_state, "", uri);
        }
    }

    /**
     * \brief Populates the palette / color editor
     * \param input_parent DOM element which will be the parent of the input elements
     * \note To be called after all the frames have been initialized
     * \note Only rules which affect at least one svg element will be shown
     */
    init_palette_editor(input_parent)
    {
        var container = this._editor_container(input_parent);

        for ( let rule of Object.values(this.recolor_rules) )
        {
            if ( rule.has_elements() )
            {
                let color_input = document.createElement("input");
                color_input.setAttribute("type", "color");
                color_input.setAttribute("title", rule.label);
                color_input.setAttribute("name", rule.name);
                color_input.setAttribute("id", "color_"+rule.name);
                color_input.addEventListener("input", function(ev){
                    rule.recolor(color_input.value);
                });
                color_input.addEventListener("change", function(){
                    this.recolor_state[color_input.name] = color_input.value;
                    if ( this.handle_query_string )
                        update_query_string(color_input.name, color_input.value, this.recolor_state);
                    if ( this.on_change )
                        this.on_change();
                }.bind(this));
                color_input.setAttribute("value", rule.current_color);

                let color_label = document.createElement("label");
                color_label.setAttribute("for", "color_"+rule.name);
                color_label.appendChild(document.createTextNode(rule.label));
                this._editor_input(container, color_label, color_input);

                if ( rule.current_color != rule.original_hex )
                    rule.recolor(rule.current_color);

                for ( let element of rule.fill_elements )
                {
                    element.addEventListener("click", () => color_input.click());
                    element.style.cursor = 'pointer';
                }
            }
        }
    }

    _editor_container(input_parent)
    {
        if ( this.table_layout )
            return input_parent.appendChild(document.createElement("table"));
        return input_parent;
    }

    _editor_input(container, label, input)
    {
        if ( this.table_layout )
        {
            var row = container.appendChild(document.createElement("tr"));
            row.appendChild(document.createElement("td")).appendChild(label);
            row.appendChild(document.createElement("td")).appendChild(input);
        }
        else
        {
            container.appendChild(input);
        }
    }

    init_feature_editor(input_parent)
    {
        var container = this._editor_container(input_parent);
        for ( let rule of Object.values(this.feature_switchers) )
        {
            let input = rule.html_input();
            input.addEventListener("change", function(){
                this.recolor_state[rule.name] = input.value;
                if ( this.handle_query_string )
                    update_query_string(input.name, input.value, this.recolor_state);
            }.bind(this));
            this._editor_input(container, rule.html_label(), input);
        }
    }


    /**
     * \brief Initializes all the frames and the palette editor
     * \note To be called after the frame documents have been loaded by the browser
     */
    init_editor(callback)
    {
        for ( var frame of document.getElementsByTagName("iframe") )
        {
            this.init_frame(frame);
        }

        this.init_palette_editor(document.getElementById('palette_editor'));

        var feature_editor = document.getElementById('feature_editor');
        if ( feature_editor )
            this.init_feature_editor(feature_editor);

        for ( let rule of Object.values(this.recolor_rules) )
        {
            if ( this.get_params[rule.name] )
                rule.current_color = this.get_params[rule.name];
            this.recolor_state[rule.name] = rule.current_color;
        }

        for ( let rule of Object.values(this.feature_switchers) )
        {
            if ( this.get_params[rule.name] )
                rule.activate(this.get_params[rule.name]);
            this.recolor_state[rule.name] = rule.active;
        }

        if ( callback )
            callback();
    }

    init_buttons()
    {
        var save_frame = document.getElementById('image_container');

        function setup_download_button(id, callback)
        {
            var save_link = document.getElementById(id);
            save_link.addEventListener("click", ev => ev.preventDefault());
            save_link.addEventListener("mousedown", function(ev)
            {
                if ( ev.button === 0 )
                {
                    ev.preventDefault();
                    callback(save_frame, null);
                }
                else
                {
                    callback(save_frame, save_link);
                }
            });
        }
        setup_download_button("save_png", this.save_frame_png.bind(this));
        setup_download_button("save_svg", this.save_frame_svg.bind(this));
    }

    /**
     * \brief Sets up the event listeners and initial recolor_state
     * \note Call after populating recolor_rules
     */
    setup_page(callback)
    {
        window.history.replaceState(this.recolor_state, "", window.location.href);

        window.addEventListener('pageshow', function(){this.init_editor(callback)}.bind(this));
        window.addEventListener("popstate", function(ev){this.apply_state(ev.state);}.bind(this));
    }

    frame_svg_filename(frame)
    {
        var svgdom = frame.contentDocument;
        var filename = svgdom.documentElement.getAttributeNS(
            "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd",
            "docname"
        );
        if ( !filename )
            filename = "image.svg";

        var aggregate_color = 0;
        for ( var color of Object.values(this.recolor_state) )
        {
            // for some reason bit shift is not as reliable as multiplication
            aggregate_color *= Math.pow(2, 24);
            aggregate_color += Number.parseInt(color.substr(1), 16);
        }
        var infix = "-" + btoa(aggregate_color).replace("+", "-").replace("/", "_");
        return filename.replace(/\.svg/, infix + ".svg");
    }

    /**
     * \brief Saves the content of the iframe to a file in the user's computer
     */
    save_frame_svg(frame, link)
    {
        var svgdom = frame.contentDocument;
        var filename = this.frame_svg_filename(frame);

        var file = svg_element_to_file(svgdom, filename);
        if ( link )
        {
            var reader = new FileReader();
            reader.onloadend = () => link.href = reader.result;
            reader.readAsDataURL(file);
        }
        else
        {
            save_file(file);
        }
    }

    /**
     * \brief Saves the given iframe as a PNG image
     * \param frame     The iframe DOM element
     * \param width     Output width in pixels
     * \param height    Output height in pixels
     * \param callback  Called upon completion. It gets passed a data url and file name.
     */
    frame_to_png(frame, width, height, callback)
    {
        var svgdom = frame.contentDocument;
        var filename = this.frame_svg_filename(frame).replace(".svg", ".png")
        svg_element_to_png(svgdom, filename, width, height, callback);
    }

    save_frame_png(frame, link)
    {
        this.frame_to_png(
            frame,
            frame.offsetWidth,
            frame.offsetHeight,
            function (data_url, filename){
                if ( link )
                    link.href = data_url;
                else
                    save_url(data_url, filename);
            }.bind(this)
        );
    }

    apply_state(state)
    {
        if ( state )
        {
            for ( var rule_name in state )
            {
                var rule = this.recolor_rules[rule_name]
                if ( rule )
                {
                    rule.recolor(state[rule_name]);
                    var element = document.getElementById("color_"+rule_name);
                    if ( element )
                        element.value = state[rule_name];
                }
                else
                {
                    rule = this.feature_switchers[rule_name];
                    if ( rule )
                    {
                        rule.activate(state[rule_name]);
                        var element = document.getElementById(rule.name);
                        if ( element )
                            element.value = state[rule_name];
                    }
                }
            }
        }
    }

}

/**
 * \brief Updates the query string and pushes the resulting uri to the browser history
 * \param key   Name of the query attribute
 * \param value Value of the query attribute (will be percent-encoded before being appended)
 */
function update_query_string(key, value, state)
{
    var uri = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var uri_value = encodeURIComponent(value);
    if ( uri.match(re) )
        uri = uri.replace(re, '$1' + key + "=" + uri_value + '$2');
    else
        uri += (uri.indexOf('?') !== -1 ? "&" : "?") + key + "=" + uri_value;

    window.history.pushState(state, "", uri);
}


/**
 * \brief Parses the query string from the window location
 */
function parse_query_string()
{
    var query_dict = {};

    for ( var token of window.location.search.substring(1).split('&') )
    {
        var assign = token.split('=');
        if ( assign.length == 1 )
            query_dict[decodeURIComponent(assign[0])] = null;
        else
            query_dict[decodeURIComponent(assign[0])] = decodeURIComponent(assign[1]);
    }

    return query_dict;
}


class FeatureSwitcher
{
    constructor(name, label, default_value="none")
    {
        this.name = name;
        this.label = label;
        this.default_value = default_value;
        this.features = {
            "none": []
        };
        this.active = default_value;
    }

    activate(feature_name)
    {
        if ( name == this.active )
            return;

        for ( var e of this.features[this.active] )
            e.style.display = "none";

        this.active = feature_name;

        for ( var e of this.features[this.active] )
            e.style.display = "inline";
    }

    append(feature, element)
    {
        if ( this.features[feature] === undefined )
            this.features[feature] = [element];
        else
            this.features[feature] = this.features[feature].concat(element);
    }

    html_input()
    {
        let switcher = this;
        let input = document.createElement("select");
        input.setAttribute("name", this.name);
        input.setAttribute("id", this.name);
        input.setAttribute("title", this.label);
        input.addEventListener("change", function(ev){
            switcher.activate(input.value);
        });

        for ( var feature of Object.keys(this.features) )
        {
            var option = document.createElement("option");
            option.setAttribute("value", feature);
            if ( feature == this.active )
                option.setAttribute("selected", "selected");
            option.appendChild(document.createTextNode(slug2title(feature)));
            input.appendChild(option);

        }

        return input;
    }

    html_label()
    {
        let label = document.createElement("label");
        label.setAttribute("for", this.name);
        label.appendChild(document.createTextNode(this.label));
        return label;
    }
}

function slug2title(slug)
{
    return slug.replace("_", " ").replace(/\b\w/g, c => c.toUpperCase());
}

// @license-end
