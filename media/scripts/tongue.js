function lerp(a, b, f)
{
    return a * (1-f) + b * f;
}

function offset_point(point, radius, angle)
{
    return {
        x: point.x + Math.cos(angle) * radius,
        y: point.y + Math.sin(angle) * radius
    };
}

class TongueGame
{
    constructor(config)
    {
        this.element = config.element;
        this.start = config.start;

        this.segments = [];

        var point = new TonguePoint(config.start.x, config.start.y, config.start.radius, this.element);

        for ( var i = 1; i <= config.segments; i++ )
        {
            var f = i / config.segments;
            var new_point = new TonguePoint(
                lerp(config.start.x, config.finish.x, f),
                lerp(config.start.y, config.finish.y, f),
                lerp(config.start.radius, config.finish.radius, f),
                this.element
            );
            this.segments.push(new TongueSegment(point, new_point, this.element));
            point = new_point;
        }
    }

    move(x, y)
    {
        var p = {x: x, y: y};
        for ( var i = this.segments.length - 1; i >= 0; i-- )
        {
            this.segments[i].move_end(p.x, p.y);
            p = this.segments[i].begin;
        }

        p = this.start;
        for ( var seg of this.segments )
        {
            seg.move_begin(p.x, p.y);
            seg.end.update_circle();
            seg.update_path();
            p = seg.end;
        }
        this.segments[0].begin.update_circle();
    }
}

class TonguePoint
{
    constructor(x, y, radius, element)
    {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        element.appendChild(this.circle);
        this.circle.setAttribute("r", this.radius);
        this.update_circle();
    }

    update_circle()
    {
        this.circle.setAttribute("cx", this.x);
        this.circle.setAttribute("cy", this.y);
    }

    offset_point(angle)
    {
        return offset_point(this, this.radius, angle);
    }
}

class TongueSegment
{
    constructor(begin, end, element)
    {
        this.begin = begin;
        this.end = end;
        var dx = this.end.x - this.begin.x;
        var dy = this.end.y - this.begin.y;
        this.angle = Math.atan2(dy, dx);
        this.length = Math.hypot(dx, dy);

        this.polygon = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
        element.appendChild(this.polygon);
        this.update_path();
    }

    move_helper(x, y, p_target, p_other, angle_offset)
    {
        p_target.x = x;
        p_target.y = y;
        this.angle = Math.atan2(
            this.end.y - this.begin.y,
            this.end.x - this.begin.x
        );
        var other = offset_point(p_target, this.length, this.angle + angle_offset);
        p_other.x = other.x;
        p_other.y = other.y;
    }

    move_end(x, y)
    {
        this.move_helper(x, y, this.end, this.begin, Math.PI);
    }

    move_begin(x, y)
    {
        this.move_helper(x, y, this.begin, this.end, 0);
    }

    update_path()
    {
        var points = [
            this.begin.offset_point(this.angle-Math.PI/2),
            this.end.offset_point(this.angle-Math.PI/2),
            this.end.offset_point(this.angle+Math.PI/2),
            this.begin.offset_point(this.angle+Math.PI/2),
        ];
        this.polygon.setAttribute("points", points.map(p => `${p.x} ${p.y}`).reduce((a, b) => a + " " + b));
    }
}
